# ----------------------------------------------------------------------
# Author: Olivier Benvenuti
# Company: Aix Sonic
# Revisions history:
# -> 0.1: First release
# ----------------------------------------------------------------------

# Libraries
from argparse import ArgumentParser, RawTextHelpFormatter
from datetime import datetime
import os
import serial
import struct
import sys
import time

# install graphical package if needed
try:
    from tqdm import tqdm
except ImportError:
    from pip._internal import main as pip_main
    pip_main(['install', '--user', 'tqdm'])
    print('Missing package "tqdm" has been installed, please re-run the script')
    sys.exit(1)

OUTPUT_FOLDER_NAME = 'output'

class CommunicationError(Exception):
    """Exception raised when communcation with the remote target
    fail.

    Attributes:
        message -- explanation of the error
    """
    
    def __init__(self, message):
        self.message = message
        
class MicrochipInterface:
    """Connect to microchip BLE device and send command to communicate
    with a remote target.

    Attributes:
        port -- Serial port to communicate with microchip device
    """
    
    COMMAND_HEADER      = 0xAA
    COMMAND_ENABLE_SCAN = 0x16
    COMMAND_ADVEVENT    = 0x70
    COMMAND_COMPLETE    = 0x80
    COMMAND_STATUS      = 0x81
    COMMAND_CONNECT     = 0x17
    COMMAND_TRANSP_UART = 0x35
    COMMAND_SEND_DATA   = 0x34
    COMMAND_DISCONNECT  = 0x1B
    
    _READ_MAX_RETRY     = 100
    
    def __init__(self, port):
        """Open serial port communication."""
        self._handle = 0
        self._device_list = {}
        self._connected_device = -1
        
        # Configure serial port
        self._uart = serial.Serial()
        self._uart.port = port
        self._uart.baudrate = 115200
        self._uart.bytesize = 8
        self._uart.parity = 'N'
        self._uart.stopbits = 1
        self._uart.xonxoff = False
        self._uart.write_timeout = 1.0
        self._uart.timeout = 1.0

        # Open serial port
        self._uart.open()

        # Verify if serial port is open
        if self._uart.isOpen():
            self._uart.flushInput()
            self._uart.flushOutput()
        else:
            raise AssertionError('Cannot open serial port')

    def get_connected_device_info(self):
        """Return the name and mac address of connected device."""
        try:
            return self._device_list[self._connected_device]
        except KeyError:
            return tuple()
            
    def _command_complete(self, sent_command):
        """Retrieve and validate command complete from device."""
        buffer = self._uart.read(4)
        (header, length, command) = struct.unpack('>BHB', buffer)
        if header != self.COMMAND_HEADER:
            raise CommunicationError('header not found.')
        if command != self.COMMAND_COMPLETE:
            raise CommunicationError('ackowledge not found (command: {0})'.format(command))
        buffer = self._uart.read(length)
        (res, status, checksum) = struct.unpack('>BBB', buffer)
        if (res != sent_command) or (status != 0x00):
            return False
        return True
            
    def _command_status(self):
        """Retrieve and validate command status from device."""
        buffer = self._uart.read(4)
        (header, length, command) = struct.unpack('>BHB', buffer)
        if header != self.COMMAND_HEADER:
            raise CommunicationError('header not found.')
        if command != self.COMMAND_STATUS:
            raise CommunicationError('status not found (command: {0})'.format(command))
        buffer = self._uart.read(length)
        (status, checksum) = struct.unpack('>BB', buffer)
        return status
        
    def _compute_checksum(self, buffer):
        """Compute checksum from bytes array and return on one byte."""
        checksum = 256
        for e in buffer[1:]:
            checksum -= e
            if checksum < 0:
                checksum += 256
        return bytes([checksum])
        
    def scan_enable(self):
        """Enable device advertissement scanning."""
        buf = struct.pack('>BHBBB', self.COMMAND_HEADER, 3,
                           self.COMMAND_ENABLE_SCAN, 1, 1)
        buf = buf + self._compute_checksum(buf)
                
        # kick off the command
        self._uart.write(buf)
                      
        # process aknowledge
        status = self._command_status()
        if status != 0x01:
            return False
        complete = self._command_complete(self.COMMAND_ENABLE_SCAN)
        if complete == False:
            return False
        return True
            
    def scan_disable(self):
        """Disable device advertissement scanning."""
        buf = struct.pack('>BHBBB', self.COMMAND_HEADER, 3,
                           self.COMMAND_ENABLE_SCAN, 0, 0)
        buf = buf + self._compute_checksum(buf)
                
        # kick off the command
        self._uart.write(buf)
            
        # process aknowledge
        status = self._command_status()
        if status != 0x01:
            return False
        complete = self._command_complete(self.COMMAND_ENABLE_SCAN)
        if complete == False:
            return False
        return True

            
    def list_devices(self):
        """
        Retrieve advertisement notification from component, discard irrelevant
        device advertisement and return a dictionary of {idx, (name, mac)}.
        """
        dev_count = 0
        self._device_list = {}
        buf = self._uart.read(4)
        while len(buf) == 4:
            (header, payload_len, command) = struct.unpack('>BHB', buf)
            if header != self.COMMAND_HEADER:
                raise CommunicationError('unexpected message from interface')
            payload = self._uart.read(payload_len)
            # looking for advert event and size of serenity name and MAC addr
            if command == self.COMMAND_ADVEVENT and payload_len == 27:
                mac = payload[2:8]
                name = payload[14:25]
                self._device_list[dev_count] = (name, mac)
                dev_count += 1
            # try to get next packet
            buf = self._uart.read(4)
            continue
        return self._device_list
        
    # Send a message to disconnect host 
    def disconnect_device(self):
        """Disconnect component from remote peer."""
        buffer = struct.pack('>BHBH', self.COMMAND_HEADER, 2,
                          self.COMMAND_DISCONNECT, self._handle)
        buffer = buffer + self._compute_checksum(buffer)
        
        # kick off the command
        self._uart.write(buffer)
        self._connected_device = -1

        
    def connect_device(self, device_number):
        """Connect component to a remote peer."""
        mac_address = [ x for x in self._device_list[int(device_number)][1]]
        buffer = struct.pack('>BHBH%sB' % len(mac_address), self.COMMAND_HEADER, 9,
                          self.COMMAND_CONNECT, 0, *mac_address)
        buffer = buffer + self._compute_checksum(buffer)
    
        self._uart.flushInput()
        # kick off the command
        self._uart.write(buffer)
            
        # process aknowledge
        status = self._command_status()
        if status != 0x02:
            print('invalid status received')
            return False
        status = self._command_status()
        if status != 0x0C:
            print('invalid status received')
            self._uart.flushInput()
            return False
            
        # retrieve device handle
        buffer = self._uart.read(4)
        (header, length, command) = struct.unpack('>BHB', buffer)
        if header != self.COMMAND_HEADER:
            raise CommunicationError('expected header not found.')
        if command != 0x71:
            raise CommunicationError('expected ackowledge not found.')
        buffer = self._uart.read(length)
        self._handle = buffer[1]
        self._connected_device = int(device_number)

    def enable_transparent_uart(self):
        """Subscribe to transparent UART characteristic (BLE spec)."""
        buf = struct.pack('>BHBHBH', self.COMMAND_HEADER, 4,
                          self.COMMAND_TRANSP_UART, 0, self._handle, 0)
        buf = buf + self._compute_checksum(buf)

        # kick off the command
        self._uart.write(buf)
        
        # process aknowledge
        complete = self._command_complete(self.COMMAND_ENABLE_SCAN)
        if complete == False:
            return False
        return True

    def send_data(self, characteristic, data):
        """Write remote peer characteritic value."""
        buffer = struct.pack('>BHBBBH', self.COMMAND_HEADER, 5 + len(data),
                          self.COMMAND_SEND_DATA, self._handle, 0, characteristic)
        if len(data) > 0:
            buffer = buffer + data
        buffer = buffer + self._compute_checksum(buffer)
        
        # kick off the command
        self._uart.write(buffer)
        
         # process aknowledge
        complete = self._command_complete(self.COMMAND_SEND_DATA)
        if complete == False:
            return False
        return True
        
    def receive_data(self):
        """Read characteristic value from transparent UART or classic answer."""
        retry = 0
        buffer = self._uart.read(4)
        while True:
            try:
                (header, length, command) = struct.unpack('>BHB', buffer)
            except struct.error:
                if retry < self._READ_MAX_RETRY:
                    time.sleep(0.1)
                    buffer = self._uart.read(4)
                    retry = retry + 1
                else:
                    raise CommunicationError('No data from remote device.')
            else:
                break
                    
        if header != self.COMMAND_HEADER:
            raise CommunicationError('expected header not found.')
        if command != 0x9A and command != 0x93:
            raise CommunicationError('expected receive command (rcv {0})'.format(command))
        buffer = self._uart.read(length)
        return buffer[1:length-1]
        
class SerenityCommunicator:
    """ High level class to send command to Serenity devices.
    
        This class parse input data, select interface and run command
        regarding input argument.
        
        Attributes:
        interface -- Communcation interface used to send and receive data
    """
    COMMAND_ARGUMENT = {
        'retrieve_agm': [ 'output' ],
        'retrieve_sig': [ 'output' ],
        'flash_device': [ 'binary' ],
    }
    
    def __init__(self, interface):
        """Save communcation interface."""
        self.interface = interface
        
    def flash_device(self, binary):    
        """Send firmware to remote peer."""
        # BLE packet size is 22 bytes, we use 2 bytes for
        # packet number and 2 others for the characteristic
        firmware_size = os.path.getsize(binary)
        packet_number = round((firmware_size / 18), None)
        if firmware_size % 18:
            packet_number = packet_number + 1

        try: 
            firmware_file = open(binary, 'rb') 
        except IOError: 
            print("cannot open output file {0}".format(binary)) 
            return
            
        # send request on two channels
        interface.send_data(0x8005, bytes.fromhex('8005'))
        interface.send_data(0x800A, bytes.fromhex('800A'))
        
        # sleep as phone app needed few time
        time.sleep(1)
        
        # retrieve packet size and packet number
        payload = interface.receive_data()
        (characteristic, battery, version) = struct.unpack('>HHH', payload)
        print('Battery level: {0} firmware version: {1}'.format(battery, version))
        
        # register to update characteristic
        interface.send_data(0x8002, bytes.fromhex('8002'))
        
        # retrieve ready message
        payload = interface.receive_data()
        
        interface.send_data(0x8007, struct.pack('>H', packet_number))
        
        for current_packet in  tqdm(range(1, packet_number+1)):
            buffer = firmware_file.read(18)
            buffer = buffer + struct.pack('>H', current_packet)
            interface.send_data(0x8007, buffer)


    def retrieve_agm(self, output):
        """Retrieve AGM data from remote peer and save it in a file."""
        
        # create output folder if it does not exist yet
        if not os.path.exists(os.path.join(os.getcwd(), OUTPUT_FOLDER_NAME)):
            os.mkdir(os.path.join(os.getcwd(), OUTPUT_FOLDER_NAME))

        # concatenate absolute filename and open it
        absolute_filename = os.path.join(os.getcwd(), OUTPUT_FOLDER_NAME, output) 
        try: 
            output_file = open(absolute_filename, 'w') 
        except IOError: 
            print("cannot open output file {0}".format(absolute_filename)) 
            return
        
        device_id = interface.get_connected_device_info()[0].decode('utf-8')
        output_file.write(f'File created on {datetime.now()} ')
        output_file.write(f'from device with BLE ID: {device_id}.\n\n')
        interface.enable_transparent_uart()
        
        # send request on two channels
        interface.send_data(0x53, bytes.fromhex('B2C2'))
        interface.send_data(0x52, bytes.fromhex('B2C2'))
        
        # retrieve gyroscope calibration
        payload = interface.receive_data()
        (confirm, gyro_cal_x, gyro_cal_y, gyro_cal_z) = struct.unpack('<Hfff', payload)

        # write calibration to file
        output_file.write('Gyroscope calibration:\n')
        output_file.write(f'X: {gyro_cal_x}\tY: {gyro_cal_y}\tZ: {gyro_cal_z}\n\n')
        
        # retrieve packet size and packet number
        payload = interface.receive_data()
        (confirm, expected_packet, packetlength) = struct.unpack('>HII', payload)
        
        # target send high level packet number but packet are split in 2 slices
        expected_packet = expected_packet * 2
        
        # write data format at the top of the file
        output_file.write("Format: ax, ay, az, gx, gy, gz, mx, my, mz, temperature, depth, dT(sec)\n")

        # loop that display progress bar with 'tqdm'
        for current_packet in tqdm(range(expected_packet)):
            # retrieve data from remote device
            payload = interface.receive_data()
            
            # parse payload in expected file format
            if not (current_packet % 2):
                # ax,ay,az,gx,gy,gz,mx,my,mz
                unpacked = struct.unpack("<hhhhhhhhh", payload[:18])
                data = ', '.join(str(value) for value in unpacked)
            else:
                # depth, dTsec
                unpacked = struct.unpack("<fff", payload)
                data += ', ' + ', '.join(str(value) for value in unpacked) + '\n'
                output_file.write(data)

        # Close output file
        output_file.close()

if __name__ == "__main__":
    # define command line arguments
    command_help_message = 'Type of action to perform:\n * '
    command_help_message += '\n * '.join(
        SerenityCommunicator.COMMAND_ARGUMENT.keys())
        
    parser = ArgumentParser(
        description = 'Connect and send command to Serenity devices through BLE.',
        formatter_class=RawTextHelpFormatter)
    parser.add_argument('--command', default='', help=command_help_message)
    parser.add_argument('--interface', default='microchip',
                        help='Bluetooth interface device (default: Microchip).')
    parser.add_argument('--port', default='COM3',
                        help='Serial port of Bluetooth device')
    parser.add_argument('--output', default="DataSave.txt",
                        help='Output file where to write incoming data.')
    parser.add_argument('--binary', help='Firmware image')
                                
    # parse input argument
    args = parser.parse_args()
    
    # is the command valid ?
    if args.command not in SerenityCommunicator.COMMAND_ARGUMENT.keys():
        print('Error: Unknown command.')
        parser.print_help(sys.stderr)
        sys.exit(1)
        
    try:
        # open host interface
        if args.interface == 'microchip':
            interface = MicrochipInterface(args.port)
        else:
            print('Error: Unknown interface.')
            parser.print_help(sys.stderr)
            sys.exit(1)
        # enable advertissing scanner
        if interface.scan_enable() == False:
            interface.scan_disable()
            if interface.scan_enable() == False:
                interface.disconnect_device()    
                raise AssertionError('cannot enable scanning mode')
            
        # look for remote serenity device
        print("Searching for available devices.")
        devices = interface.list_devices()
        
        # stop scanning
        interface.scan_disable()
        
        if not devices:
            print('no device found')
            sys.exit(1)
            
        print('Device(s) found')
        for (idx, (name, mac)) in devices.items():
            print('\t{0}. {1} ({2})'.format(idx, name,
                ':'.join('{:02X}'.format(x) for x in mac)))
        dev_number = input('Select device: ')

        try:
            print("Connect to ", devices[int(dev_number)][0])
        except (ValueError, KeyError):
            print("Invalid device selected.")
            sys.exit(1)
        
        interface.connect_device(dev_number)
        
        # initialise master Serenity communicator
        communicator = SerenityCommunicator(interface)
        
        # convert argument to function and parameters
        command_args = {}
        process = getattr(communicator, args.command)

        for a in communicator.COMMAND_ARGUMENT[args.command]:
            command_args[a] = getattr(args, a)
        
        process(**command_args)

    except CommunicationError as e:
        print('Communication error: {0}'.format(e.message))
        interface.disconnect_device()    
        parser.print_help(sys.stderr)
        sys.exit(1)
