#include "DiveFileGenerator.h"


//The constructor generate a divefile with specified parameters
DiveFileGenerator::DiveFileGenerator(float speed, float frequency, float depth1, float depth2 )
{
    float step;
    QFile file("tempo_dive_file.txt");

    if(!file.open(QIODevice::WriteOnly | QIODevice::Text)) //Open the file or create it
    {
        cout<< "Erreur fichier" << endl;
        return;
    }

    step = speed / (60 * frequency); //Step calculus

    QTextStream flux(&file);

    if(depth1 >= depth2)
    {

        while(depth1 >= depth2) //Until we reach the to go depth
        {
            flux << depth1 << endl;
            depth1 -= step;
        }
    }
    else
    {

        while(depth1 <= depth2) //Until we reach the to go depth
        {
            flux << depth1 << endl;
            depth1 += step;
        }
    }

    file.close();
}


