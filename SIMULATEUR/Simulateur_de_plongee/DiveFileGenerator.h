#ifndef DIVEFILEGENERATOR_H
#define DIVEFILEGENERATOR_H

#include <iostream>
#include <QFile>
#include <QTextStream>

using namespace std;

class DiveFileGenerator
{
public:
    DiveFileGenerator(float speed, float frequency, float depth1, float depth2);
};

#endif // DIVEFILEGENERATOR_H
