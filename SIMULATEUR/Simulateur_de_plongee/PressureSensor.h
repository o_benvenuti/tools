#ifndef PRESSURESENSOR_H
#define PRESSURESENSOR_H


#include <stdio.h>
#include <sys/types.h> // open
#include <sys/stat.h>  // open
#include <fcntl.h>     // open
#include <unistd.h>    // read/write usleep
#include <stdlib.h>    // exit
#include <inttypes.h>  // uint8_t, etc
#include <linux/i2c-dev.h> // I2C bus definitions
#include <sys/ioctl.h>
#include <iostream>


#define MY_BASE 2222

using namespace std;


class PressureSensor
{
private:
    int fd;
    int ads_address = 0x48;
    uint8_t buf[10];
    int16_t val;


public:
    PressureSensor();
    ~PressureSensor();
    float getPressure();

};

#endif // PRESSURESENSOR_H
