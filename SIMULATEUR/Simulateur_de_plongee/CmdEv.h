#ifndef CMDEV_H
#define CMDEV_H

#include <pigpio.h>
#include <iostream>
#include <unistd.h>
#include <bcm2835.h>

using namespace std;

#define ENA 13
#define ENB 21

#define IN1 19
#define IN2 26
#define IN3 16
#define IN4 20

class CmdEv
{
public:
    CmdEv();
    ~CmdEv();

    //Commandes EV sortie d'air
    void open_outev();
    void open_outev_for(float milliseconds);
    void close_outev();
    void close_outev_for(unsigned int milliseconds);

    //Commandes EV entree d'air
    void open_inev();
    void open_inev_for(float milliseconds);
    void close_inev();
    void close_inev_for(unsigned int milliseconds);

    //Commandes générales
    void close_allev();
};

#endif // CMDEV_H
