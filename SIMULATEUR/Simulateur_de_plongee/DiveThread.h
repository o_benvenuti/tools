#ifndef DIVETHREAD_H
#define DIVETHREAD_H

#include <QThread>
#include <iostream>
#include <CmdEv.h>
#include <PressureSensor.h>
#include <DiveFileGenerator.h>



using namespace std;

struct divedata
{
    float vr1, vr2, vr3, vr4;
    int nbp, prof, pp1, pp2, pp3, dp1, dp2, dp3;
};

class DiveThread : public QThread
{
    Q_OBJECT
public:
   explicit DiveThread(QObject *parent = 0);
   ~DiveThread();
   void run();
   void customdepth(float depth);
   void calibration(float pression_depart, int prof_arrivee);
   void increase_pressure();
   void decrease_pressure();
   void purge();
   void start_dive();
   void start_custom_dive();
   float precise_pressure(float);
   void getpressureloop(float config_depth);
   void speedtest();
   void tab_generator(float * dive_tab);


   bool Stop, functionlaunch;
   int function; //Select function to execute in thread

   float pressure = 0.0;
   CmdEv *ev;
   PressureSensor *pressure_sensor;
   struct divedata actualDataDive;
   QString th_divepath;

public slots :
   void stop_dive();
signals :
   void sendpressure(float);

public slots :

};

#endif // DIVETHREAD_H
