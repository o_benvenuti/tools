#include <QApplication>
#include <QPushButton>
#include <QWidget>
#include "MaFenetre.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

   //Création de la fenetre
    MaFenetre  fenetre;
    fenetre.show();

    return app.exec();
}
