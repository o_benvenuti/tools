#ifndef DEF_MAFENETRE
#define DEF_MAFENETRE

#include <QApplication>
#include <QWidget>
#include <QPushButton>
#include <QLCDNumber>
#include <QSlider>
#include <QLabel>
#include <QSize>
#include <QLineEdit>
#include <QIntValidator>
#include <QLayout>
#include <QFile>
#include <QTextStream>
#include <QFileDialog>
#include <iostream>
#include "FenetrePaliers.h"
#include "FenetreVitesses.h"
#include "CmdEv.h"
#include "PressureSensor.h"
#include <stdio.h>
#include <thread>
#include <string>
#include "DiveThread.h"


using namespace std;

class MaFenetre : public QWidget
{

    Q_OBJECT

public:
    MaFenetre();


public slots:

    void config_paliers();
    void config_vitesses();
    void aff_prof();
    void aff_nbpaliers();
    void re_init();
    void re_init2();
    void depth0();
    void depth20();
    void depth40();
    void depth60();
    void depth80();
    void depth100();
    void increase_pressure();
    void decrease_pressure();
    void purge1();
    void start_dive();
    void start_custom_dive();
    void lcddisplay(float);
    void on_emergency_stop();
    void speedtest();

private:

    //Variables declaration
    int nbp=0,prof=0,pp1=0,pp2=0,pp3=0,dp1=0,dp2=0,dp3=0;
    float prof_actu=0.0, prof_sensor = 0.0,pression_init = 0.0, vr1=0.0,vr2=0.0,vr3=0.0,vr4=0.0;
    bool test1=0,test2=0,test3=0;

    //Declaration of UI Objects
    QFont *m_police1;
    QPushButton *m_button1,*m_button2,*m_button3,*m_button4,*m_button5,*button_ok1,*button_ok2,*button_ok3, *button_test_etch, *pressure0,
                *pressure20, *pressure40, *pressure60, *pressure80, *pressure100, *boutonplus, *boutonmoins, *purge, *boutonexplo, *stop_button, *speedtest_button;
    QLCDNumber *m_lcd1;
    QLabel *m_label1,*m_label2,*m_label3,*m_label4,*label_p1,*label_p2,*label_p3,*label_p4,*label_p5,*label_p6,*label_p7,*label_p8,*label_p9,*label_p10,*label_p11, *label_pressure;
    QLabel *m_label_img, *label_unit_pressure;
    QPixmap *m_pixmap;
    QLineEdit *m_line1,*m_line2,*m_line3;
    QValidator *m_valid;
    FenetrePaliers *fenetre_p;
    FenetreVitesses *fenetre_v;

    //Declaration of control Objects
    CmdEv *ev;
    PressureSensor *pressure_sensor;
    DiveThread *dthread;


    void closeEvent(QCloseEvent * bar);

signals :
    void do_emergency_stop();

};

#endif
