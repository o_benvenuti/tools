#include "PressureSensor.h"

PressureSensor::PressureSensor()
{

    if ((fd = open("/dev/i2c-1", O_RDWR)) < 0) {
        printf("Error: Couldn't open device! %d\n", fd);
    }

    if (ioctl(fd, I2C_SLAVE, ads_address) < 0) {
        printf("Error: Couldn't find device on address!\n");
    }

    val=0;
    // set config register and start conversion
    // AIN0 and GND, 4.096v, 128s/s
    buf[0] = 1;    // config register is 1
    buf[1] = 0xc1;
    buf[2] = 0x85;

}

PressureSensor::~PressureSensor()
{
    close(fd);
}



float PressureSensor::getPressure()
{
    val=0.0;
    // set config register and start conversion
    // AIN0 and GND, 4.096v, 128s/s
    buf[0] = 1;    // config register is 1
    buf[1] = 0xc1;
    buf[2] = 0x85;
    if (write(fd, buf, 3) != 3) {
         perror("Write to register 1");
         return -1;
     }

    usleep(10000);

    // wait for conversion complete
    do {
         if (read(fd, buf, 2) != 2) {
              perror("Read conversion");
              return -1;
            }
          } while (!(buf[0] & 0x80));

   buf[0] = 0;   // conversion register is 0

   if (write(fd, buf, 1) != 1) {
            perror("Write register select");
            return -1;
   }
   if (read(fd, buf, 2) != 2) {
           perror("Read conversion");
           return -1;
   }

   val = (int16_t)buf[0]*256 + (uint16_t)buf[1];
   return val*0.00037536621;
}

