#ifndef FENETREVITESSES_H
#define FENETREVITESSES_H

#include <QObject>
#include <QWidget>
#include <QPushButton>
#include <iostream>
#include <QLineEdit>
#include <QLabel>
#include <QFont>
#include <QValidator>
#include <QDialog>
#include <unistd.h>

using namespace std;

class FenetreVitesses : public QDialog
{

    Q_OBJECT

public:
    FenetreVitesses(int nbpaliers);
    int get_speed(int num_vit);
    int fv_nbp;

public slots:
    void get_data();

private:
    int v1,v2,v3,v4;
    QFont *pol1;
    QLabel *l1,*l2,*l3,*l4,*l5,*l6,*l7,*l8,*l9,*l10,*l11;
    QPushButton *b1,*b2,b3,*b4;
    QLineEdit *le1,*le2,*le3,*le4,*le5,*le6;
    QValidator *vali1;


};

#endif // FENETREVITESSES_H
