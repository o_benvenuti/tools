#include "MaFenetre.h"

//Dimensions fenetre
#define LONF 1000
#define LARF 600

//Dimensions logo
#define LONL 1181
#define LARL 527

MaFenetre::MaFenetre() : QWidget()
{
    dthread = new DiveThread;


    //Windows configuration
    setFixedSize(LONF, LARF);
    setStyleSheet("background-color:black");
    m_police1=new QFont("Courrier",10,QFont::Bold,false);

    //Logo configuration
    m_pixmap = new QPixmap("/home/pi/Projets/Simulateur_de_plongee/logo_serenity.png");
    *m_pixmap=m_pixmap->scaled(LONL/5,LARL/5,Qt::IgnoreAspectRatio);

    m_label_img = new QLabel(this);
    m_label_img->setPixmap(*m_pixmap);
    m_label_img->move( (LONF/2-(LONL/5)/2) ,LARF-120);



    //LCD configuration


    m_label1 = new QLabel(tr("Profondeur\n  Actuelle:"),this);
    m_label1->setStyleSheet("color: white;");
    m_label1->move(900,50);

    m_lcd1 = new QLCDNumber(this);
    m_lcd1->move(888,100);
    m_lcd1->setGeometry(900,100,50,30);
    m_lcd1->display(0);



    //Line Edit and label configuration

    //"Prof. Initiale" Part
    m_valid= new QIntValidator(0,100,this);
    m_line1 = new QLineEdit(this);
    m_line1->setStyleSheet("background-color: white");
    m_line1->setValidator(m_valid);
    m_line1->setCursorPosition(0);
    m_line1->move(LONF/10,LARF/10);

    m_label2 = new QLabel(tr("Prof. initiale :"),this);
    m_label2->setStyleSheet("color: white;");
    m_label2->move(LONF/10,LARF/10-30);


    //"Nb de Paliers" Part
    m_valid= new QIntValidator(0,3,this);
    m_line2 = new QLineEdit(this);
    m_line2->setStyleSheet("background-color: white");
    m_line2->setValidator(m_valid);
    m_line2->move(LONF/10,LARF/10+100);
    m_label3 = new QLabel(tr("Nb de paliers :"),this);
    m_label3->setStyleSheet("color: white;");
    m_label3->move(LONF/10,LARF/10+70);





    //Parameters display

    label_p1 = new QLabel(tr("Parametres plongee :"),this);
    label_p1->setStyleSheet("color: white");
    label_p1->move(LONF/2,50);

    label_pressure = new QLabel(tr("Profondeur(m):"),this);
    label_pressure->setStyleSheet("color: white");
    label_pressure->move(350, 50);



    label_p2 = new QLabel("Prof. Initiale: ",this);
    label_p2->setGeometry(LONF/2,100,125,20);
    label_p2->setStyleSheet("color: white");

    label_p3 = new QLabel("Nombre de paliers : ",this);
    label_p3->setGeometry(LONF/2,125,200,20);
    label_p3->setStyleSheet("color: white");

    label_p4 = new QLabel("",this);
    label_p4->setGeometry(LONF/2,160,400,20);
    label_p4->setStyleSheet("color: white");

    label_p5 = new QLabel("",this);
    label_p5->setGeometry(LONF/2,185,400,20);
    label_p5->setStyleSheet("color: white");

    label_p6 = new QLabel("",this);
    label_p6->setGeometry(LONF/2,210,400,20);
    label_p6->setStyleSheet("color: white");

    label_p7 = new QLabel("",this);
    label_p7->setGeometry(LONF/2,235,405,20);
    label_p7->setStyleSheet("color: white");

    label_p8 = new QLabel("",this);
    label_p8->setGeometry(LONF/2,260,400,20);
    label_p8->setStyleSheet("color: white");

    label_p9 = new QLabel("",this);
    label_p9->setGeometry(LONF/2,285,400,20);
    label_p9->setStyleSheet("color: white");

    label_p10 = new QLabel("",this);
    label_p10->setGeometry(LONF/2,310,400,20);
    label_p10->setStyleSheet("color: white");

    label_p11 = new QLabel("",this);
    label_p11->setGeometry(LONF/2,335,400,20);
    label_p11->setStyleSheet("color: white");

    label_unit_pressure = new QLabel("m",this);
    label_unit_pressure->move(960, 107);
    label_unit_pressure->setStyleSheet("color: white");



    //Button configuration

    m_button1 = new QPushButton("GO",this);
    m_button1->setStyleSheet("background-color: white");
    m_button1->setFont(*m_police1);
    m_button1->move(LONF/1.2,LARF-125);

    speedtest_button = new QPushButton("Speed Test",this);
    speedtest_button->setStyleSheet("background-color: white");
    speedtest_button->setFont(*m_police1);
    speedtest_button->move(LONF-300,LARF-125);

    stop_button = new QPushButton("STOP",this);
    stop_button->setStyleSheet("background-color: red");
    stop_button->setFont(*m_police1);
    stop_button->move(LONF-300,LARF-75);

    boutonexplo = new QPushButton("LANCER FICHIER\nPLONGEE",this);
    boutonexplo->setStyleSheet("background-color: white");
    boutonexplo->setFont(*m_police1);
    boutonexplo->move(LONF/1.2-26,LARF-75);

    m_button2 = new QPushButton("Configurer les\n  paliers",this);
    m_button2->setStyleSheet("background-color: white");
    m_button2->setFont(*m_police1);
    m_button2->move(100,LARF-250);

    m_button3 = new QPushButton("Configurer les\nvitesses de\nremontees",this);
    m_button3->setStyleSheet("background-color: white");
    m_button3->setFont(*m_police1);
    m_button3->move(100,LARF-200);

    m_button4 = new QPushButton("Modifier",this);
    m_button4->setStyleSheet("background-color: white");
    m_button4->setFont(*m_police1);
    m_button4->move(LONF/10-85,LARF/10+105);

    m_button5 = new QPushButton("Modifier",this);
    m_button5->setStyleSheet("background-color: white");
    m_button5->setFont(*m_police1);
    m_button5->move(LONF/10-85,LARF/10+5);

    button_ok1 = new QPushButton("OK",this);
    button_ok1->setStyleSheet("background-color: white");
    button_ok1->setFont(*m_police1);
    button_ok1->setGeometry(LONF/10+170,LARF/10+5,25,25);

    button_ok2 = new QPushButton("OK",this);
    button_ok2->setStyleSheet("background-color: white");
    button_ok2->setFont(*m_police1);
    button_ok2->setGeometry(LONF/10+170,LARF/10+105,25,25);

    boutonplus = new QPushButton("+",this);
    boutonplus->setStyleSheet("background-color: white");
    boutonplus->setFont(*m_police1);
    boutonplus->setGeometry(460, 50, 25, 25);

    boutonmoins = new QPushButton("-",this);
    boutonmoins->setStyleSheet("background-color: white");
    boutonmoins->setFont(*m_police1);
    boutonmoins->setGeometry(320, 50, 25, 25);

    pressure0 = new QPushButton("0",this);
    pressure0->setStyleSheet("background-color: white");
    pressure0->setFont(*m_police1);
    pressure0->move(363, 100);

    pressure20 = new QPushButton("20",this);
    pressure20->setStyleSheet("background-color: white");
    pressure20->setFont(*m_police1);
    pressure20->move(363, 135);

    pressure40 = new QPushButton("40",this);
    pressure40->setStyleSheet("background-color: white");
    pressure40->setFont(*m_police1);
    pressure40->move(363, 170);

    pressure60 = new QPushButton("60",this);
    pressure60->setStyleSheet("background-color: white");
    pressure60->setFont(*m_police1);
    pressure60->move(363, 205);

    pressure80 = new QPushButton("80",this);
    pressure80->setStyleSheet("background-color: white");
    pressure80->setFont(*m_police1);
    pressure80->move(363, 240);

    pressure100 = new QPushButton("100",this);
    pressure100->setStyleSheet("background-color: white");
    pressure100->setFont(*m_police1);
    pressure100->move(363, 275);

    purge = new QPushButton("PURGE",this);
    purge->setStyleSheet("background-color: white");
    purge->setFont(*m_police1);
    purge->move(100, 500);


    //We desable butto that cannot be use at launch
    m_button1->setEnabled(false);
    m_button2->setEnabled(false);
    m_button3->setEnabled(false);

    //Connections
    QObject::connect(button_ok1,SIGNAL(clicked()),this,SLOT(aff_prof()));
    QObject::connect(button_ok2,SIGNAL(clicked()),this,SLOT(aff_nbpaliers()));
    QObject::connect(m_button1,SIGNAL(clicked()),this,SLOT(start_dive()));
    QObject::connect(m_button2,SIGNAL(clicked()),this,SLOT(config_paliers()));
    QObject::connect(m_button3,SIGNAL(clicked()),this,SLOT(config_vitesses()));
    QObject::connect(m_button4,SIGNAL(clicked()),this,SLOT(re_init()));
    QObject::connect(m_button5,SIGNAL(clicked()),this,SLOT(re_init2()));
    QObject::connect(boutonexplo,SIGNAL(clicked()),this,SLOT(start_custom_dive()));
    QObject::connect(dthread, SIGNAL(sendpressure(float)), this, SLOT(lcddisplay(float)));
    QObject::connect(pressure0,SIGNAL(clicked()),this,SLOT(depth0()));
    QObject::connect(pressure20,SIGNAL(clicked()),this,SLOT(depth20()));
    QObject::connect(pressure40,SIGNAL(clicked()),this,SLOT(depth40()));
    QObject::connect(pressure60,SIGNAL(clicked()),this,SLOT(depth60()));
    QObject::connect(pressure80,SIGNAL(clicked()),this,SLOT(depth80()));
    QObject::connect(pressure100,SIGNAL(clicked()),this,SLOT(depth100()));
    QObject::connect(boutonplus,SIGNAL(clicked()),this,SLOT(increase_pressure()));
    QObject::connect(boutonmoins,SIGNAL(clicked()),this,SLOT(decrease_pressure()));
    QObject::connect(purge,SIGNAL(clicked()),this,SLOT(purge1()));
    QObject::connect(this,SIGNAL(do_emergency_stop()),dthread,SLOT(stop_dive()));
    QObject::connect(stop_button,SIGNAL(clicked()),this,SLOT(on_emergency_stop()));
    QObject::connect(speedtest_button,SIGNAL(clicked()),this,SLOT(speedtest()));


    dthread->function=11; //We set the function thread
    dthread->start();//Thread Start
}

//Function use to test speed, when wall increase the pressure unti 6bar and then decreasepressure until 0 bar
void MaFenetre::speedtest()
{
    dthread->functionlaunch = 1;
    usleep(125000);
    dthread->function = 12; //We set the function to execute
    dthread->start();      //Launch the thread to execute function
}

//Function call when u press the stop button
void MaFenetre::on_emergency_stop()
{
    emit do_emergency_stop(); //Signal emit that close all ev and stop the thread
}

//Function that display depth
void MaFenetre::lcddisplay(float depth)
{
    m_lcd1->display(depth*10);
}


//Display the depth
void MaFenetre::aff_prof()
{
    QString sprof = m_line1->text();
    QString text_prof ="Prof. Initiale : " + m_line1->text();
    label_p2->setText(text_prof);
    prof=sprof.toInt();
    m_line1->setEnabled(false);
    button_ok1->setEnabled(false);
    test1=true;

    if( (test1==true) && ( (test2==true)||(nbp==0)) && test3==true)
    {
        m_button1->setEnabled(true);
    }

}
 //Display Stop number
void MaFenetre::aff_nbpaliers()
{
    QString nbpaliers =m_line2->text();
    QString text_nbpaliers ="Nombre de paliers : " + m_line2->text();
    label_p3->setText(text_nbpaliers);
    nbp=nbpaliers.toInt();
    label_p4->clear();
    m_button2->setEnabled(true);
    m_button3->setEnabled(true);
    m_line2->setEnabled(false);
    button_ok2->setEnabled(false);

}

//Open a new window to configure the diving stop
void MaFenetre::config_paliers()
{
    QString buffer;
    QString nb_paliers = m_line2->text(); //We the stop number in the line edit
    nbp=nb_paliers.toInt();
    if(nbp!=0)
    {
        fenetre_p = new FenetrePaliers(nbp); //We open a new windows for configuration
        fenetre_p->exec();
        QString para_p;
        buffer = buffer.number(fenetre_p->get_duree(1));
        dp1=buffer.toInt();

        pp1 =fenetre_p->get_prof(1);

        para_p = "Premier Palier a " + para_p.number(fenetre_p->get_prof(1)) + " metres pendant " + para_p.number(dp1) + " minutes"; //Creation of sentence to display
        label_p4->setText(para_p);

            if(nbp>1)
            {
                QString para_p2;
                buffer = buffer.number(fenetre_p->get_duree(2));
                dp2=buffer.toInt();
                pp2=fenetre_p->get_prof(2);
                para_p2 = "Second Palier a " + para_p2.number(fenetre_p->get_prof(2)) + " metres pendant " + para_p2.number(fenetre_p->get_duree(2)) + " minutes";
                label_p5->setText(para_p2);
            }

            if(nbp>2)
            {
                QString para_p3;
                buffer = buffer.number(fenetre_p->get_duree(3));
                dp3=buffer.toInt();
                pp3=fenetre_p->get_prof(3);
                para_p3 = "Troisieme Palier a " + para_p3.number(fenetre_p->get_prof(3)) + " metres pendant " + para_p3.number(fenetre_p->get_duree(3)) + " minutes";
                label_p6->setText(para_p3);
            }

            test2 = true;
    }
    else
    {
        //We reset
        pp1=0.0;
        pp2=0.0;
        pp3=0.0;
    }


    if( (test1==true) && ( (test2==true)||(nbp==0)) && test3==true) //We test if all parameters have been set, if yes, we able the access to "GO" button
    {
        m_button1->setEnabled(true);
    }
}

void MaFenetre::config_vitesses()
{
    cout << "Ok config vitesse" << endl;
    fenetre_v= new FenetreVitesses(nbp);
    fenetre_v->exec();
    cout << "TEST  " << nbp << endl;

        if (nbp==0)
        {
            vr1=fenetre_v->get_speed(1);
            QString para_v = "Vitesse entre la prof. initiale et la surface : " + para_v.number(vr1) + " m/min";
            label_p7->setText(para_v);
        }
        else
        {
            vr1=fenetre_v->get_speed(1);
            QString para_v1 = "Vitesse entre la prof. initiale et le palier 1 : " + para_v1.number(vr1) + " m/min";
            label_p8->setText(para_v1);
        }


        if(nbp>1)
        {
            vr2=fenetre_v->get_speed(2);
            QString para_v2 = "Vitesse entre le palier 1 et le palier 2 : " + para_v2.number(vr2) + " m/min";
            label_p9->setText(para_v2);
        }

        if(nbp>2)
        {
            vr3=fenetre_v->get_speed(3);
            QString para_v3 = "Vitesse entre le palier 2 et le palier 3 : " + para_v3.number(vr3) + " m/min";
            label_p10->setText(para_v3);
        }

        vr4=fenetre_v->get_speed(4);
        QString para_v4 = "Vitesse entre le palier 1 et la surface : " + para_v4.number(vr4) + " m/min";
        QString para_v5 = "Vitesse entre le palier 2 et la surface : " + para_v5.number(vr4) + " m/min";
        QString para_v6 = "Vitesse entre le palier 3 et la surface : " + para_v6.number(vr4) + " m/min";

        switch (nbp)
        {

            case 1:
                label_p11->setText(para_v4);
                break;

            case 2:
                label_p11->setText(para_v5);
                break;

            case 3:
                label_p11->setText(para_v6);
                break;

            default :
                break;
        }

    test3 = true;

    if( (test1==true) && ( (test2==true)||(nbp==0)) && test3==true)
    {
        m_button1->setEnabled(true);
    }

}

void MaFenetre::re_init()
{

    m_line2->setEnabled(true);
    button_ok2->setEnabled(true);
    m_button2->setEnabled(false);
    m_button3->setEnabled(false);
    m_button1->setEnabled(false);
    test2=false;
    test3=false;
}

void MaFenetre::re_init2() //bouton reinit de prof.initiale
{
    m_line1->setEnabled(true);
    button_ok1->setEnabled(true);
    m_button1->setEnabled(false);

    test1=false;

}

void MaFenetre::start_dive()
{
    dthread->functionlaunch=1;
    usleep(125000);

    //Copy data in thread
    dthread->actualDataDive.dp1 = dp1;
    dthread->actualDataDive.dp2 = dp2;
    dthread->actualDataDive.dp3 = dp3;
    dthread->actualDataDive.pp1 = pp1;
    dthread->actualDataDive.pp2 = pp2;
    dthread->actualDataDive.pp3 = pp3;
    dthread->actualDataDive.vr1 = vr1;
    dthread->actualDataDive.vr2 = vr2;
    dthread->actualDataDive.vr3 = vr3;
    dthread->actualDataDive.vr4 = vr4;
    dthread->actualDataDive.prof = prof;
    dthread->actualDataDive.nbp = nbp;

    //Start thread
    dthread->function = 9;
    dthread->start();
}

void MaFenetre::start_custom_dive()
{
    dthread->functionlaunch=1;
    usleep(125000);
    cout << "test" << endl;
    QString divepath = QFileDialog::getOpenFileName(this, tr("Open FIle"), "/home/pi/Desktop" , tr("Text files (*.txt)") );

    cout << "Test : " << divepath.toStdString() << endl;

    dthread->th_divepath = divepath;

    dthread->function = 10;
    dthread->start();
}




void MaFenetre::depth0()
{
    dthread->functionlaunch=1;
    usleep(125000);
    dthread->function = 0; //We set the function to execute
    dthread->start();      //Launch the thread to execute function
}

void MaFenetre::depth20()
{
    dthread->functionlaunch=1;
    usleep(125000);
    dthread->function = 1;
    dthread->start();
}

void MaFenetre::depth40()
{
    dthread->functionlaunch=1;
    usleep(125000);
    dthread->function = 2;
    dthread->start();
}

void MaFenetre::depth60()
{
    dthread->functionlaunch=1;
    usleep(125000);
    dthread->function = 3;
    dthread->start();
}

void MaFenetre::depth80()
{
    dthread->functionlaunch=1;
    usleep(125000);
    dthread->function = 4;
    dthread->start();
}

void MaFenetre::depth100()
{
    dthread->functionlaunch=1;
    usleep(125000);
    dthread->function = 5;
    dthread->start();
}

void MaFenetre::increase_pressure()
{
    dthread->functionlaunch=1;
    usleep(125000);
    dthread->function = 6;
    dthread->start();
}

void MaFenetre::decrease_pressure()
{
    dthread->functionlaunch=1;
    usleep(125000);
    dthread->function = 7;
    dthread->start();
}

void MaFenetre::purge1()
{
    dthread->functionlaunch=1;
    usleep(125000);
    dthread->function = 8;
    dthread->start();
}

void MaFenetre::closeEvent(QCloseEvent *bar)
{
   cout << "Fermeture du Programme" << endl;
   ev->~CmdEv();
   pressure_sensor->~PressureSensor();
   sleep(1);
    QWidget::closeEvent(bar);
}



/*void MaFenetre::sleep_ms(int milliseconds)
{
    usleep(milliseconds * 1000);
}


void MaFenetre::test()
{
    for(int i = 0; i < 30; i++)
    {
    cout << "Thread actif" << endl;
    sleep(1);
    }
}*/
