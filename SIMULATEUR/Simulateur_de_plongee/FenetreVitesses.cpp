#include "FenetreVitesses.h"
#define LONF2 300
#define LARF2 350

FenetreVitesses::FenetreVitesses(int nbpaliers) : QDialog()
{
    fv_nbp=nbpaliers; // On initialise notre variable nbp qui va nous servir pour d'autre fonction
    cout << "Nb de palier :" << fv_nbp << endl;
    v1 = v2 = v3 = v4 = 0;
    //Config generale
    setFixedSize(LONF2, LARF2);
    setStyleSheet("background-color:black");
    pol1 = new QFont("Courrier",11,QFont::Bold,false);

    b1 = new QPushButton("OK",this);
    b1->setStyleSheet("background-color: white");
    b1->setFont(*pol1);
    b1->setGeometry(LONF2/2-20,LARF2-50,40,25);

    l1 = new QLabel("Configuration des vitesses",this);
    l1->move(50,0);
    l1->setStyleSheet("color: white");
    l1->setFont(*pol1);




    le1= new QLineEdit(this);
    le1->setGeometry(0,68,100,30);
    le1->setStyleSheet("background-color: white");
    vali1 = new QIntValidator(1,100,this);
    le1->setValidator(vali1);

        if(nbpaliers==0) //Prise en compte du cas ou il n'y a pas de palier car cela change la config
        {
            l2 = new QLabel("Vitesse Prof Initiale -> Surface :",this);
            l2->setFont(*pol1);
            l2->move(0,50);
            l2->setStyleSheet("color: white");
            l2->show();
        }

        if(nbpaliers>0)
        {
            l3 = new QLabel("Vitesse Prof Initiale -> Palier 1 :",this);
            l3->setFont(*pol1);
            l3->move(0,50);
            l3->setStyleSheet("color: white");
            l3->show();



        }

        if(nbpaliers>1)
        {

            l4 = new QLabel("Vitesse Palier 1 -> Palier 2 :",this);
            l4->setFont(*pol1);
            l4->move(0,110);
            l4->setStyleSheet("color: white");
            l4->show();

            le2= new QLineEdit(this);
            le2->setGeometry(0,128,100,30);
            le2->setStyleSheet("background-color: white");
            vali1 = new QIntValidator(1,100,this);
            le2->setValidator(vali1);
        }

        if(nbpaliers>2)
        {
            l5 = new QLabel("Vitesse Palier 2 -> Palier 3 :",this);
            l5->setFont(*pol1);
            l5->move(0,170);
            l5->setStyleSheet("color: white");
            l5->show();

            le3= new QLineEdit(this);
            le3->setGeometry(0,188,100,30);
            le3->setStyleSheet("background-color: white");
            vali1 = new QIntValidator(1,100,this);
            le3->setValidator(vali1);
        }

        switch(nbpaliers)
        {
        case 1:
            l6 = new QLabel("Vitesse Palier 1 -> Surface :",this);
            l6->setFont(*pol1);
            l6->move(0,110);
            l6->setStyleSheet("color: white");
            l6->show();

            le4= new QLineEdit(this);
            le4->setGeometry(0,128,100,30);
            le4->setStyleSheet("background-color: white");
            vali1 = new QIntValidator(1,100,this);
            le4->setValidator(vali1);
            break;

        case 2:
            l6 = new QLabel("Vitesse Palier 2 -> Surface :",this);
            l6->setFont(*pol1);
            l6->move(0,170);
            l6->setStyleSheet("color: white");
            l6->show();

            le4= new QLineEdit(this);
            le4->setGeometry(0,188,100,30);
            le4->setStyleSheet("background-color: white");
            vali1 = new QIntValidator(1,100,this);
            le4->setValidator(vali1);
            break;

        case 3:
            l6 = new QLabel("Vitesse Palier 3 -> Surface :",this);
            l6->setFont(*pol1);
            l6->move(0,230);
            l6->setStyleSheet("color: white");
            l6->show();

            le4= new QLineEdit(this);
            le4->setGeometry(0,248,100,30);
            le4->setStyleSheet("background-color: white");
            vali1 = new QIntValidator(1,100,this);
            le4->setValidator(vali1);
            break;

        default :
            break;
        }

    QObject::connect(b1,SIGNAL(clicked()),this,SLOT(get_data()));
    QObject::connect(b1,SIGNAL(clicked()),this,SLOT(hide()));
}


void FenetreVitesses::get_data()
{
    bool ok;
    cout << fv_nbp << endl;
    QString buff = le1->text();
    v1=buff.toInt(&ok,10);


        if(fv_nbp>1)
        {
            cout << "ok4" << endl;
            //usleep(5000000);
            buff = le2->text();

            v2 =buff.toInt(&ok,10);
        }

        if(fv_nbp>2)
        {
           buff =  le3->text();
           v3 = buff.toInt(&ok,10);
        }

        if(fv_nbp!=0)
        {
           buff = le4->text();
           v4 = buff.toInt(&ok,10);
        }

}


int FenetreVitesses::get_speed(int num_vit)
{
    switch (num_vit)
    {
    case 1:
        return v1;
        break;

    case 2:
        return v2;
        break;

    case 3:
        return v3;
        break;
    case 4:
        return v4;
        break;

    default:
        return 0;
        break;
    }
}
