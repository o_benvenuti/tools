#include "CmdEv.h"


//Constructor initialize gpio
CmdEv::CmdEv()
{
   int rc=0;

    rc=gpioInitialise();
    if(rc==PI_INIT_FAILED)
    {
        cout << "Erreur d'initialisation\n" <<endl;
    }

    gpioSetMode(IN2, PI_OUTPUT);
    gpioSetMode(IN1, PI_OUTPUT);
    gpioSetMode(ENA, PI_OUTPUT);
    gpioSetMode(ENB, PI_OUTPUT);
    gpioSetMode(IN3, PI_OUTPUT);
    gpioSetMode(IN4, PI_OUTPUT);


    gpioWrite(ENA,0);
    gpioWrite(IN1,0);
    gpioWrite(IN2,0);
    gpioWrite(ENB,0);
    gpioWrite(IN3,0);
    gpioWrite(IN4,0);
}

CmdEv::~CmdEv()
{
    gpioWrite(ENA, 0);
    gpioWrite(IN1, 0);
    gpioWrite(IN2, 0);

    gpioWrite(ENB, 0);
    gpioWrite(IN3, 0);
    gpioWrite(IN4, 0);

    gpioTerminate();
}

//COMMANDE EV ENTREE AIR
void CmdEv::open_inev()
{
    gpioWrite(ENB,1);
    gpioWrite(IN3,0);
    gpioWrite(IN4,1);

}

void CmdEv::close_inev()
{
    gpioWrite(ENB,0);
    gpioWrite(IN3,0);
    gpioWrite(IN4,0);
}

void CmdEv::close_inev_for(unsigned int milliseconds)
{
    close_inev();
    usleep(milliseconds*1000);
    open_inev();
}

void CmdEv::open_inev_for(float milliseconds)
{
    open_inev();
    usleep(milliseconds*1000);
    close_inev();
}

//COMMANDE EV SORTIE AIR
void CmdEv::open_outev()
{
    gpioWrite(ENA,1);
    gpioWrite(IN1,0);
    gpioWrite(IN2,1);
}

void CmdEv::close_outev()
{
    gpioWrite(ENA,0);
    gpioWrite(IN1,0);
    gpioWrite(IN2,0);
}

void CmdEv::close_outev_for(unsigned int milliseconds)
{
    close_outev();
    usleep(milliseconds*1000);
    open_outev();
}

void CmdEv::open_outev_for(float milliseconds)
{
    open_outev();
    usleep(milliseconds*1000);
    close_outev();
}

void CmdEv::close_allev()
{
    close_inev();
    close_outev();
}

