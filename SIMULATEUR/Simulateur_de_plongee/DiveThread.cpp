#include "DiveThread.h"
#include <QtCore>

DiveThread::DiveThread(QObject *parent) : QThread(parent)
{
    functionlaunch = 0;
    ev = new CmdEv();
    pressure_sensor = new PressureSensor();
}

DiveThread::~DiveThread()
{

    delete pressure_sensor;
    delete ev;
}

//Function launch at the thread start
void DiveThread::run()
{
    cout << "FONCTION CHOISI : " << function << endl;

    //The thread launch the function set by "funtion"
    switch(function)
    {
        case 0 :
            customdepth(0.0);
            break;
        case 1 :
            customdepth(20.0);
            break;
        case 2:
            customdepth(40.0);
            break;
        case 3 :
            customdepth(60.0);
            break;
        case 4 :
            customdepth(80.0);
            break;
        case 5 :
            customdepth(100.0);
        case 6 :
            increase_pressure();
            break;
        case 7 :
            decrease_pressure();
            break;
        case 8 :
            purge();
            break;
        case 9 :
            start_dive();
            break;
        case 10 :
            start_custom_dive();
            break;
        case 11 :
            getpressureloop(0.0);
            break;
        case 12 :
            speedtest();


        default :
            break;
    }


}

//The default function that will be launch if no other function are call by user
void DiveThread::getpressureloop(float depth_config)
{    
    while(functionlaunch == 0)
    {
        pressure = pressure_sensor->getPressure(); //Getting pressure
        if(depth_config != 0)
        {
            if( (pressure*10) > (depth_config + 1) )
            {
                if(pressure > 9)
                {
                    ev->open_outev_for(100);
                }
                ev->open_outev_for(30);
            }
            else if ( (pressure*10) < (depth_config - 1) )
            {
                ev->open_inev_for(30);
            }
        }
        emit sendpressure(pressure); //We emit a signal for the main thread to send him the pressure
        usleep(100000);
    }
}

//Function call when a stop is needed
void DiveThread::stop_dive()
{
    cout << "STOP" << endl;
    ev->close_allev(); //close all ev before terminate the thread
    this->terminate(); //Stop Thread
}

//Go to the depth pass in parameters
void DiveThread::customdepth(float depth)
{
        float start_pressure, pressure_togo;
        pressure = start_pressure = pressure_sensor->getPressure();
        pressure_togo = depth/10.0; //Convertion depth to pressure (m to bar)

        if(pressure < pressure_togo) //If the actual pressure is lower than the pressure we want to go
        {
            while(pressure <  pressure_togo) //As long as the actual pressure is lower that the demandes pressure
            {
                pressure = pressure_sensor->getPressure();
                ev->open_inev_for(30); //Open In Ev to increase pressure
                usleep(100000);
                emit sendpressure(pressure);
            }
        }
        else //Else it means that the actual pressure is higher than the pressure we want to go
        {
            ev->close_inev();
            while(pressure > pressure_togo )
            {
                emit sendpressure(pressure);
                pressure = pressure_sensor->getPressure();
                ev->open_outev();
                emit sendpressure(pressure);
            }

        }

        calibration(start_pressure, depth); //calibration to gain precision

        emit sendpressure(pressure);
        ev->close_allev();
        functionlaunch = 0; //When the function end we have to tell that no function are running
        getpressureloop(depth); //launch the thread
}


//Make a fast open/close on IN ev, that perform a pressure increase
void DiveThread::increase_pressure()
{
    /*pressure = pressure_sensor->getPressure();

    ev->open_inev();*/
    if(pressure > 1) //If the pressure is under 1 bar we have to increase the open time due to pressure
    {
    ev->open_inev_for(15);
    }
    else ev->open_inev_for(11);

    usleep(200000); //Delay to compense the sensor lag
    pressure = pressure_sensor->getPressure();
    emit sendpressure(pressure);
    functionlaunch = 0;
    getpressureloop(0);
}

//Make a fast open/close on OUT ev, that perform a pressure decrease
void DiveThread::decrease_pressure()
{
    DiveFileGenerator test(10, 1, 100.0, 0.0);
    pressure = pressure_sensor->getPressure();
    ev->open_outev_for(50);
    emit sendpressure(pressure);
    functionlaunch = 0;
    getpressureloop(0);
}

//Function call to purge the pipe between the bottle and the tank
//WARNING : BEFORE CALLING THIS FUNCTION TAKE CARE THAT BOTTLE IS CLOSE
void DiveThread::purge()
{
    cout << "PURGE" << endl;
    ev->open_inev();    //Open all ev at the same time
    ev->open_outev();
    sleep(5);   //Wait 5 seconds to be sure all pressure is gone

    cout << "FIN PURGE VOUS POUVEZ DEBRANCHEZ LA BOUTEILLE" << endl;
    ev->close_allev();
    functionlaunch = 0;
    getpressureloop(0);
}

void DiveThread::start_dive()
{
    int rc = 0, i = 0;
    bool p1done = 0, p2done = 0, p3done = 0;
    float t_delay = 0.0, pression_init = 0.0, dive_tab[10000];
    unsigned long timetowait = 0;

    cout <<"Palier 1 : "<< actualDataDive.pp1<< endl;
    cout <<"Palier 2 : "<< actualDataDive.pp2<< endl;
    cout <<"Palier 3 : "<< actualDataDive.pp3<< endl;
    cout << "Vitesse :" << actualDataDive.vr3 <<endl;
    cout << "Nombre de palier :" << actualDataDive.nbp << endl;
    tab_generator(dive_tab);

    cout << "Sauvegarde donnees de la plongee..." << endl;
    QFile fichier("/home/pi/Desktop/Simulateur/sauvegarde.txt");

    if(fichier.open(QIODevice::WriteOnly))
    {
         QTextStream f(&fichier);
         f << "$" << actualDataDive.nbp <<',' << actualDataDive.prof << ',' << actualDataDive.pp1 << ',' << actualDataDive.pp2<< ','
           << actualDataDive.pp3 << ',' << actualDataDive.dp1 << ',' << actualDataDive.dp2 << ',' << actualDataDive.dp3 << ',' ;
    }


     cout << "CALIBRATION PROFONDEUR INITIAL" << endl;

    pressure= pressure_sensor->getPressure();
    while(pressure < ( (actualDataDive.prof/10) - 0.3) )
    {
        pressure= pressure_sensor->getPressure();
        ev->open_inev_for(15);
        usleep(100000);
        emit sendpressure(pressure);
    }


    while(pressure > ( (actualDataDive.prof/10) + 0.3) )
    {
        pressure = pressure_sensor->getPressure();
        ev->open_outev_for(15);
        usleep(50000);
        emit sendpressure(pressure);
    }



     cout << "CALIBRATION PROFONDEUR INITIAL TERMINE" << endl;
     cout << "DEBUT DE LA PLONGEE DANS 5 SECONDES" << endl;
     usleep(5000000);


     while(dive_tab[i] > 0) //As long as we reach the surface
     {
         timetowait = 1000 - precise_pressure(dive_tab[i]);  //Calul the time that we have to wait (the time vary with delay needed to go at the pressure)
         printf("Time to wait : %ld\n", timetowait * 1000);
         if(timetowait > 0)
         {
            usleep(timetowait * 1000);
         }
         else cout << "No DELAY" << endl;

         i++;
     }
    /*rc = gpioInitialise();
    if(rc == PI_INIT_FAILED)
    {
        cout << "Erreur d'initialisation\n" <<endl;
    }

    pression_init =  actualDataDive.prof/10.0; //Unit convertion
    printf("Pression Initial : %f bar", pression_init);

    pressure = pressure_sensor->getPressure();
    emit sendpressure(pressure);
    while(pressure < (actualDataDive.prof/10.0)) //As long the pressure is lower than
    {
        pressure = pressure_sensor->getPressure();
        printf("Pression capteur : %.4f bar\n", pressure);
        ev->open_inev_for(30);
        usleep(100000);
        emit sendpressure(pressure);
    }

    cout << "CALIBRATION PROFONDEUR INITIAL" << endl;
    if(pressure < (actualDataDive.prof/10.0) )
    {
        while(pressure < (actualDataDive.prof/10.0) )
        {
            pressure= pressure_sensor->getPressure();
            ev->open_inev_for(30);
            usleep(100000);
            emit sendpressure(pressure);
        }
    }
    else
    {
        while(pressure > (actualDataDive.prof/10.0) )
        {
            pressure = pressure_sensor->getPressure();
            ev->open_outev_for(30);
            usleep(50000);
            emit sendpressure(pressure);
        }
    }


    cout << "CALIBRATION PROFONDEUR INITIAL TERMINE" << endl;
    sleep(2);
    cout << "Debut de la plongee" << endl;

    t_delay=60/(actualDataDive.vr1*10);
    cout << t_delay<<endl;
   



  while(pressure > 0.01)
  {
    pressure = pressure_sensor->getPressure();
    printf("Pression capteur : %.4f bar\n", pressure);

    if( (actualDataDive.nbp>0) && (p1done ==0) )
    {
       if(  (pressure*10.0 > (actualDataDive.pp1-0.3) ) &&  (pressure*10.0 < (actualDataDive.pp1+0.3) ) )
       {
           cout << "Palier 1 en cours..." << endl;
           sleep(actualDataDive.dp1*60);
           if(actualDataDive.nbp == 1)
           {
               t_delay = 60/(actualDataDive.vr4*10);
           }
           else t_delay = 60/(actualDataDive.vr2*10);

           p1done = 1;
       }

    }

    if( (actualDataDive.nbp > 1) && (p2done == 0) )
    {
       if(  (pressure*10.0 > (actualDataDive.pp2-0.3) ) &&  (pressure*10.0 < (actualDataDive.pp2+0.3) ) )
       {
           cout << "Palier 2 en cours..." << endl;
           sleep(actualDataDive.dp2*60);
           if(actualDataDive.nbp == 2)
           {
           t_delay = 60/(actualDataDive.vr4*10);
           }
           else t_delay = 60/(actualDataDive.vr3*10);

           p2done = 1;
       }
    }

    if( (actualDataDive.nbp > 2) && (p3done == 0) )
    {
       if(  (pressure*10.0 > (actualDataDive.pp3-0.3) ) &&  (pressure*10.0 < (actualDataDive.pp3+0.3) ) )
       {
           cout << "Palier 3 en cours..." << endl;
           sleep(actualDataDive.dp3*60);
           t_delay=60/(actualDataDive.vr4*10);
           cout << t_delay<<endl;

           p3done = 1;
       }
    }
   emit sendpressure(pressure);

   ev->open_outev_for(20);
   usleep(t_delay*1000000);

   }*/


    cout << "Fin de la plongee" << endl;
    functionlaunch = 0;
    getpressureloop(0);
}

void DiveThread::tab_generator(float * dive_tab)
{
    float step = 0.0, prof_actu = actualDataDive.prof, frequency = 1.0;
    int i = 0;

    cout << "Nombre de palier" << actualDataDive.nbp << endl;

    cout << "VR1 :" << actualDataDive.vr1 << endl;
    cout << "VR2 :" << actualDataDive.vr2 << endl;
    cout << "VR3 :" << actualDataDive.vr3 << endl;
    cout << "VR4 :" << actualDataDive.vr4 << endl;
    cout << "DP1 :" << actualDataDive.dp1 << endl;

    sleep(5);

    step = actualDataDive.vr1 / (60 * frequency); //Step calculus



    if(actualDataDive.nbp != 0) //if there 1 stop or more
    {

        while(prof_actu > actualDataDive.pp1) //Until we reach the first stop
        {
            prof_actu -= step;
            dive_tab[i] = prof_actu;
            cout << dive_tab[i] << endl;
            i++;
        }

        while(actualDataDive.dp1 != 0) //Here we generate the first stop
        {
            for(int n = 0; n < 60 ; n++)
            {
                dive_tab[i] = actualDataDive.pp1;
                cout << dive_tab[i] << endl;
                i++;
            }

            actualDataDive.dp1--;
        }

        if(actualDataDive.nbp > 1) //if there is 2 stop or more
        {
            step = actualDataDive.vr2 / (60 * frequency);
            while(prof_actu > actualDataDive.pp2) //Until we reach the second stop
            {
                prof_actu -= step;
                dive_tab[i] = prof_actu;
                cout << dive_tab[i] << endl;
                i++;
            }

            while(actualDataDive.dp2 != 0) // Here we generate the seconde stop
            {
                for(int n = 0; n < 60 ; n++)
                {
                    dive_tab[i] = actualDataDive.pp2;
                    cout << dive_tab[i] << endl;
                    i++;
                }

                actualDataDive.dp2--;
            }


            if(actualDataDive.nbp > 2) //if there is 3 stop
            {
                step = actualDataDive.vr3 / (60 * frequency);
                while(prof_actu > actualDataDive.pp3) //Until we reach the third stop
                {
                    prof_actu -= step;
                    dive_tab[i] = prof_actu;
                    cout << dive_tab[i] << endl;
                    i++;
                }

                while(actualDataDive.dp3 != 0) // Here we generate the third stop
                {
                    for(int n = 0; n < 60 ; n++)
                    {
                        dive_tab[i] = actualDataDive.pp3;
                        cout << dive_tab[i] << endl;
                        i++;
                    }

                    actualDataDive.dp3--;
                }
            }
        }

        step = actualDataDive.vr4 / (60 * frequency); //Step calculus
        while(prof_actu > 0) //Until we reach surface
        {
            prof_actu -= step;
            dive_tab[i] = prof_actu;
            cout << dive_tab[i] << endl;
            i++;
        }
    }

    else //if there is no stop
    {
        while(prof_actu > 0)
        {
            prof_actu -= step;
            dive_tab[i] = prof_actu;
            cout << dive_tab[i] << endl;
            i++;
        }
    }
}

//Read a data file that contain a recoring of a real dive and reproduce the condition in the tank (Function Work at 1Hz)
void DiveThread::start_custom_dive()
{
    float pressure_togo = 0.0, init_pressure = 0.0, timetowait = 0.0;


    QFile divefile(th_divepath);

    if(!divefile.open(QIODevice::ReadOnly)) //Open file
    {
        cout  << "Erreur ouverture" << endl;
    }

    QTextStream  dive_content(&divefile);
    QString line = dive_content.readLine();
    init_pressure = (line.toFloat()/10);
    cout << "CALIBRATION PROFONDEUR INITIAL" << endl;

        while(pressure < (init_pressure-0.3) )
        {
            pressure= pressure_sensor->getPressure();
            ev->open_inev_for(15);
            usleep(100000);
            emit sendpressure(pressure);
        }


        while(pressure > (init_pressure+0.3) )
        {
            pressure = pressure_sensor->getPressure();
            ev->open_outev_for(15);
            usleep(50000);
            emit sendpressure(pressure);
        }



    cout << "CALIBRATION PROFONDEUR INITIAL TERMINE" << endl;
    cout << "DEBUT DE LA PLONGEE DANS 5 SECONDES" << endl;
    usleep(5000000);

    while(!dive_content.atEnd()) //As long as the file is not over
    {
        line = dive_content.readLine(); //We read the line (each pressure is record in 1 line)
        pressure_togo = line.toFloat(); //pressure_togo take the value of the number stock at current line
        timetowait = 1000.0 - precise_pressure(pressure_togo);  //Calul the time that we have to wait (the time vary with delay needed to go at the pressure)
        cout << pressure_togo << endl;

        printf("Time to wait : %ld\n", (unsigned long) (timetowait*1000));
        if(timetowait > 0)
        {
           usleep( (unsigned long) (timetowait *1000));
        }
        else cout << "No DELAY" << endl;
    }

    divefile.close();
    functionlaunch = 0;
    getpressureloop(0);
}

void DiveThread::speedtest()
{
    if(pressure < 6.0 )
    {
        while(pressure < 6.0 )
        {
            pressure= pressure_sensor->getPressure();
            ev->open_inev_for(30);
            usleep(100000);
            emit sendpressure(pressure);
        }
    }
    else
    {
        while(pressure > 6.0 )
        {
            pressure = pressure_sensor->getPressure();
            ev->open_outev_for(30);
            usleep(50000);
            emit sendpressure(pressure);
        }
    }

    functionlaunch = 0;
    getpressureloop(0);
}


float DiveThread::precise_pressure(float pressure_togo)
{   
    float timepass = 0.0;
    printf("Pression demande : %f\n", pressure_togo);
    pressure_togo = pressure_togo/10; // m -> bar
    pressure = pressure_sensor->getPressure();
    emit sendpressure(pressure);


    while( (pressure < (pressure_togo ) ))
    {
        if(pressure < 1.0 )
        {
            ev->open_inev_for(11);
            timepass = timepass + 111;
        }
        else
        {
            ev->open_inev_for(15);
            timepass = timepass + 115;
        }
        usleep(100000);
        pressure = pressure_sensor->getPressure();
        emit sendpressure(pressure);
    }

    while( (pressure > (pressure_togo ) ))
    {
        ev->open_outev_for(11);
        usleep(80000);
        pressure = pressure_sensor->getPressure();
        emit sendpressure(pressure);
        timepass = timepass + 91;
    }

    //printf("Pression finale : %f\n", pressure_sensor->getPressure());

    emit sendpressure(pressure);
    printf("TEMPS UTILISE : %f\n", timepass);
    return timepass;
}

void DiveThread::calibration(float pression_depart, int prof_arrivee)
{
    cout << "Calibration" << endl;

    //CALIBRATION AVEC PRESSION DE DEPART 8 BAR
    if( (pression_depart >7.0) && (pression_depart < 9.0) )
    {
        switch(prof_arrivee)
        {
            case 20 :
            ev->open_outev_for(375);
            break;

            case 40 :
            ev->open_outev_for(200);
            break;

            case 60 :
            ev->open_outev_for(80);
            break;

        default :
            break;
        }
    }

    //CALIBRATION AVEC PRESSION DE DEPART 6 BAR
    else if( (pression_depart >5.0) && (pression_depart < 7.0) )
    {
        switch(prof_arrivee)
        {
            case 20 :
            ev->open_outev_for(300);
            break;

            case 40 :
            ev->open_outev_for(110);
            break;

        default :
            break;
        }
    }

    //CALIBRATION AVEC PRESSION DE DEPART 4 BAR
    else if( (pression_depart >3.0) && (pression_depart < 5.0) )
    {
        switch(prof_arrivee)
        {
            case 20 :
            cout << "test" << endl;
            ev->open_outev_for(180);
            break;

        default :
            break;
        }
    }

}

