#include "FenetrePaliers.h"

#define LONF2 300
#define LARF2 350
FenetrePaliers::FenetrePaliers(int nbpaliers) : QDialog()
{
    nbp=nbpaliers; //Copie du nombre de palier qui servir
    prof_1 = 0, prof_2 = 0, prof_3 = 0; //Initialisation a 0 zero des paliors
    d1 = 0, d2 = 0, d3 = 0;
    //Config generale
    setFixedSize(LONF2, LARF2);
    setStyleSheet("background-color:black");
    pol1 = new QFont("Courrier",11,QFont::Bold,false);

    l1 = new QLabel("Configuration des paliers",this);
    l1->move(50,0);
    l1->setStyleSheet("color: white");
    l1->setFont(*pol1);

    //Config bouton OK
    b1 = new QPushButton("OK",this);
    b1->setStyleSheet("background-color: white");
    b1->setFont(*pol1);
    b1->setGeometry(LONF2/2-20,LARF2-50,40,25);

    //Config Palier 1
    l2 = new QLabel("Palier 1:",this);
    l2->setFont(*pol1);
    l2->move(0,50);
    l2->setStyleSheet("color: white");

    l3 = new QLabel("Profondeur (m):",this);
    l3->move(0,68);
    l3->setStyleSheet("color: white");

    l4 = new QLabel("Duree (minutes):",this);
    l4->move(0,92);
    l4->setStyleSheet("color: white");

    le1= new QLineEdit(this);
    le1->setGeometry(125,68,38,20);
    le1->setStyleSheet("background-color: white");
    vali1 = new QIntValidator(1,100,this);
    le1->setValidator(vali1);

    le2= new QLineEdit(this);
    le2->setGeometry(125,92,38,20);
    le2->setStyleSheet("background-color: white");
    vali2 = new QIntValidator(1,60,this);
    le2->setValidator(vali2);



    if(nbpaliers>1)
    {
        //Config Palier 2
        l5 = new QLabel("Palier 2:",this);
        l5->setFont(*pol1);
        l5->move(0,125);
        l5->setStyleSheet("color: white");

        l6 = new QLabel("Profondeur (m):",this);
        l6->move(0,143);
        l6->setStyleSheet("color: white");

        l7 = new QLabel("Duree (minutes):",this);
        l7->move(0,167);
        l7->setStyleSheet("color: white");

        le3= new QLineEdit(this);
        le3->setGeometry(125,143,38,20);
        le3->setStyleSheet("background-color: white");
        le3->setValidator(vali1);

        le4 = new QLineEdit(this);
        le4->setGeometry(125,167,38,20);
        le4->setStyleSheet("background-color: white");
        le4->setValidator(vali2);
    }

    if(nbpaliers>2)
    {
        l8 = new QLabel("Palier 3:",this);
        l8->setFont(*pol1);
        l8->move(0,200);
        l8->setStyleSheet("color: white");

        l9 = new QLabel("Profondeur (m):",this);
        l9->move(0,218);
        l9->setStyleSheet("color: white");

        l10 = new QLabel("Duree (minutes):",this);
        l10->move(0,242);
        l10->setStyleSheet("color: white");

        le5= new QLineEdit(this);
        le5->setGeometry(125,218,38,20);
        le5->setStyleSheet("background-color: white");
        le5->setValidator(vali1);

        le6 = new QLineEdit(this);
        le6->setGeometry(125,242,38,20);
        le6->setStyleSheet("background-color: white");
        le6->setValidator(vali2);
    }

QObject::connect(b1,SIGNAL(clicked()),this,SLOT(get_data()));
QObject::connect(b1,SIGNAL(clicked()),this,SLOT(hide()));

}

int FenetrePaliers::get_prof(int num_palier)
{
    switch (num_palier)
    {
    case 1:
        return prof_1;
        break;

    case 2:
        return prof_2;
        break;

    case 3:
        return prof_3;
        break;

    default:
        return 0;
        break;
    }
}

int FenetrePaliers::get_duree(int num_palier)
{
    switch (num_palier)
    {
    case 1:
        return d1;
        break;

    case 2:
        return d2;
        break;

    case 3:
        return d3;
        break;

    default:
        return 0; //Retourne 0 si le nombre de paliers n'est pas valide
        break;
    }
}

void FenetrePaliers::get_data()
{
    bool ok;
    QString buff = le1->text();
    prof_1=buff.toInt(&ok,10);

    buff = le2->text();
    d1 =buff.toInt(&ok,10);

        if(nbp>1)
        {
            buff = le3->text();
            prof_2 =buff.toInt(&ok,10);

            buff = le4->text();
            d2 =buff.toInt(&ok,10);
        }

        if(nbp>2)
        {
            buff = le5->text();
            prof_3 =buff.toInt(&ok,10);

            buff = le6->text();
            d3 =buff.toInt(&ok,10);
        }

}
