QT += widgets core
QMAKE_CXXFLAGS += -Wshadow
INCLUDEPATH += /usr/include/
LIBS += -lpigpio -lrt -lpthread -lbcm2835

SOURCES += \
    main.cpp \
    MaFenetre.cpp \
    FenetrePaliers.cpp \
    FenetreVitesses.cpp \
    CmdEv.cpp \
    PressureSensor.cpp \
    DiveThread.cpp \
    DiveFileGenerator.cpp

HEADERS += \
    MaFenetre.h \
    FenetrePaliers.h \
    FenetreVitesses.h \
    CmdEv.h \
    PressureSensor.h \
    DiveThread.h \
    DiveFileGenerator.h

