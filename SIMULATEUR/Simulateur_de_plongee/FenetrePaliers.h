#ifndef FENETREPALIERS_H
#define FENETREPALIERS_H

#include <QApplication>
#include <QObject>
#include <QWidget>
#include <QPushButton>
#include <iostream>
#include <QLineEdit>
#include <QLabel>
#include <QFont>
#include <QValidator>
#include <QDialog>


using namespace std;

class FenetrePaliers : public QDialog
{

    Q_OBJECT


public:
    FenetrePaliers(int nbpaliers);
    int get_prof(int num_palier);
    int get_duree(int num_palier);

public slots:
    void get_data();


private:

    int nbp,prof_1,prof_2,prof_3,d1,d2,d3;
    QPushButton *b1,*b2,b3,*b4;
    QLineEdit *le1,*le2,*le3,*le4,*le5,*le6;
    QLabel *l1,*l2,*l3,*l4,*l5,*l6,*l7,*l8,*l9,*l10,*l11;
    QFont *pol1;
    QValidator *vali1,*vali2;


};



#endif // FENETREPALIERS_H
