import re
import serial
import struct
import sys
import time

class MicrochipProgrammer(object): 
    """Class used to program and configure BM71 BLE device""" 
 
    # BM71 communication constants 
    EVT_HCI = chr(0x04) 
    EVT_ACL = chr(0x02) 
    CMD_TYPE_CMD = 0x01 
    CMD_TYPE_DTA = 0x02 
    HCI_ID_READ     = 0x0110 
    HCI_ID_ERASE    = 0x0112 
    HCI_ID_WRITE    = 0x0111 
    HCI_ID_CONTINUE = 0x0001 
    HCI_OPCODE_CONNECT = 0x0405 
    HCI_OPCODE_DISCONN = 0x0406 
 
    #--------------------------------------------------------- 
    # FUNCTION OVERLOADING 
    #--------------------------------------------------------- 
    def __init__(self, port): 
        """Initialise attributes and open the serial port""" 
        self.handle = 0 
        self.to_send = '' 
 
        self.__uart = serial.Serial() 
        self.__uart.port = port 
        self.__uart.baudrate = 115200 
        self.__uart.bytesize = 8 
        self.__uart.parity = 'N' 
        self.__uart.stopbits = 1 
        self.__uart.xonxoff = False 
        self.__uart.write_timeout = 10.0 
        self.__uart.timeout = 1.0 
 
        # Open serial port 
        self.__uart.open() 
        if not self.__uart.isOpen():  
            raise AssertionError('Cannot open port "%s"' % device) 
 
    #--------------------------------------------------------- 
    # PRIVATE 
    #--------------------------------------------------------- 
    def __send(self, command, payload): 
        """Send connect command to device and retrieve its handle""" 
        # construct packet header regarding its type 
        if command == self.CMD_TYPE_CMD: 
            self.to_send += struct.pack("<B", self.CMD_TYPE_CMD) 
        elif command == self.CMD_TYPE_DTA: 
            self.to_send += struct.pack("<BHH", self.CMD_TYPE_DTA, self.handle, len(payload)) 
        else: 
            print "invalid command type" 
            return '' 
 
        # kick off header plus its payload 
        self.to_send += payload 
        ret = self.__uart.write(self.to_send) 
        if ret != len(self.to_send): 
            raise AssertionError("UART write failed") 
        self.to_send = '' 

        # expect acknowledge 
        return self.__receive(command) 
 
    def __receive(self, command=None): 
        length = 0 
        data = self.__uart.read(1) 
        if len(data) != 1: 
            print "No data from target" 
            return '' 
        # read payload regarding its type 
        if data == self.EVT_HCI: 
            data = self.__uart.read(2) 
            if len(data) != 2: 
                raise AssertionError("Read HCI data error") 
            (type, length) = struct.unpack("<BB", data) 
            # these type are HCI header, read the next packet 
            # which will be the actual response 
            if type == 0xf or type == 0x13: 
                data = self.__uart.read(length) 
                # TODO find out what's the fail value on first byte 
                if (len(data) != length): 
                    raise AssertionError("Read status error len {0}".format(len(data))) 
                return self.__receive() 
        elif data == self.EVT_ACL: 
            data = self.__uart.read(4) 
            if len(data) != 4: 
                raise AssertionError("Read ACL data error") 
            (handle, length) = struct.unpack("<HH", data) 
        else: 
            raise AssertionError('Unexpected event type {0}'.format(ord(data))) 
        # and read payload 
        payload = self.__uart.read(length) 
        if len(payload) != length: 
            raise AssertionError("Failed to read payload") 
        return payload 
 
    #--------------------------------------------------------- 
    # PUBLIC 
    #--------------------------------------------------------- 
    def connect(self): 
        """Send connect command to device and retrieve its handle""" 
        unknown = struct.pack("<BBBB", 0x01, 0x05, 0x10, 0x00)  
        self.__uart.write(unknown) 
        response = self.__uart.read(14) 
        unknown = struct.pack("<BBBBBBBBBBB", 0x01, 0x33, 0x0C, 0x07, 0x01, 0x00, 0x02, 0x03, 0x00, 0x04, 0x00) 
        self.__uart.write(unknown) 
        response = self.__uart.read(7) 
        padlen = 13 
        payload = struct.pack("<HB%ux" % padlen, self.HCI_OPCODE_CONNECT, padlen) 
        response = self.__send(MicrochipProgrammer.CMD_TYPE_CMD, payload) 
        if response == None: 
            raise AssertionError("Invalid return from target") 
        (status, handle) = struct.unpack('<BH', response[:3]) 
        if status != 0: 
            raise AssertionError("cannot connect to target") 
        self.handle = handle 
        unknown = struct.pack("<BBBBBBBBBBBB", 0x02, 0xFF, 0x0F, 0x07, 0x00, 0x00, 0x01, 0x03, 0x00, 0x03, 0x00, 0x00) 
        self.__uart.write(unknown) 
        response = self.__uart.read(19) 
        time.sleep(10) 
 
    def disconnect(self): 
        """Send disconnect command to the device"""
        packet = struct.pack("<HBHB", self.HCI_OPCODE_DISCONN, 3, self.handle, 0) 
        self.__send(MicrochipProgrammer.CMD_TYPE_CMD, packet) 
 
    def erase_flash(self, address, length): 
        payload = struct.pack("<HHBBII", self.HCI_ID_ERASE,
                              0x0A, 0x3, 0, address, length) 
        response = self.__send(self.CMD_TYPE_DTA, payload) 
        if response == None: 
            raise AssertionError("Invalid return from target") 
        time.sleep(100/1000) 
 
    def init_transfer(self, burst_size, address, file_size): 
        self.burst_size = burst_size 
        self.packet = struct.pack('<HHBBII', self.HCI_ID_WRITE, 
                                  0x80F6, 3, 0, address, file_size) 
        self.remaining = burst_size - len(self.packet) 
 
    def feed_and_process(self, buffer): 
        if len(self.packet) == 0: 
            # rebuild a new packet of alternate data 
            self.packet = struct.pack('<HH', self.HCI_ID_CONTINUE, 0x80F4) 
            self.remaining = self.burst_size - 2 - len(self.packet) 
        if len(buffer) > self.remaining: 
            self.packet = self.packet + buffer[0:self.remaining] 
            self.__send(self.CMD_TYPE_DTA, self.packet) 
            time.sleep(0.05) 
            self.packet = bytearray() 
            self.feed_and_process(buffer[self.remaining:]) 
        elif len(buffer) == self.remaining: 
            self.packet = self.packet + buffer[0:self.remaining] 
            self.__send(self.CMD_TYPE_DTA, self.packet) 
            time.sleep(0.05) 
            self.packet = bytearray() 
        else: 
            self.packet = self.packet + buffer 
            # new size is packet size minus header and size 
            packet_len = len(self.packet) - 4 
            tmp = list(self.packet) 
            tmp[2] = struct.pack('<B', packet_len) 
            tmp[3] = struct.pack('<B', 0) 
            self.packet = ''.join(tmp) 
            self.remaining = self.remaining - len(buffer) 
            self.__send(self.CMD_TYPE_DTA, self.packet) 
            time.sleep(1) 
            self.packet = bytearray() 
 
    def flash_device(self, files): 
        for idx, file in enumerate(files): 
            with open(file, "r") as f: 
                buffer = '' 
                chunksize = 0 
                expected_address = 0 
                try: 
                    # get burst size from this very first line 
                    line = f.readline() 
                    while line != "": 
                        # discard first line 
                        if line == ":020000040000FA\n": 
                            line = f.readline() 
                            continue 
                        # end of file, align buffer on 8 bytes 
                        if line == ":00000001FF\n": 
                            if (expected_address % 8): 
                                chunksize += (8 - (expected_address % 8)) 
                                buffer += "\xff" * (8 - (expected_address % 8)) 
                            break 
                        # first byte is the size 
                        linesize = int(line[1:3], 16) 
                        chunksize += linesize 
                        # second and third bytes are address 
                        current_address = int(line[3:7], 16) 
                        if current_address != expected_address: 
                            # if there is jump since last data line pad 
                            # the gap of memory with 0xFF 
                            buffer += "\xff" * (current_address - expected_address) 
                            chunksize += (current_address - expected_address) 
                        expected_address = current_address + linesize 
                        # fourth byte is an unknown data 
                        buffer += line[9:(linesize*2)+9].decode("hex") 
                        # and then actual data (with checksum as last byte) 
                        line = f.readline() 
                finally: 
                    f.close() 
                     
                # send constructed buffer 
                self.init_transfer(0xFA, (idx << 16), chunksize) 
                 
                # this function use recursivity, we need to 
                # rework it - why not - add some user interface 
                self.feed_and_process(buffer)       
 
    def configure_device(self, file): 
        with open(file, "r") as f: 
            buffer = '' 
            chunksize = 0 
            expected_address = 0 
            try: 
                # get burst size from this very first line 
                line = f.readline() 
                while line != '': 
                    # discard comment lines 
                    if line[0] == ";" or line[0] == '\n': 
                        line = f.readline() 
                        continue 
                    # first byte is the size (discard it) 
                    tmp = ''.join(re.findall(r'[0-9A-F]+', line[8:], re.I)) 
                    buffer += tmp.decode('hex') 
                    line = f.readline() 
            finally: 
                f.close() 
                 
            # send constructed buffer 
            self.init_transfer(0xFA, 0x34000, len(buffer)) 
             
            # this function use recursivity, we need to 
            # rework it - why not - add some user interface 
            self.feed_and_process(buffer)
            
class SerenityBuilder():
    BREAKPOINT_UID = 'main:write_uid' 
    BREAKPOINT_BLE = 'main:flash_ble' 
    BREAKPOINT_END = 'main:test_end' 
 
    def __init__(self): 
        self.msb_uid = 0 
        self.lsb_uid = 0
        self.ble_done = False
        self.program = ''
        # attach handler to breakpoint event 
        gdb.events.stop.connect(self.process_breakpoint) 
     
    def ble_processed(self):
        self.ble_done = True
       
    def start_production(self, str_uid): 
        '''Format UID and run production process'''
        if str_uid[0:2] == '10':
            self.program = 'binaries/SERENITY_v003_18F6C956'
        elif str_uid[0:2] == '11':
            self.program = 'binaries/SERENITY_v102_A1F0CE5A'

        (self.msb_uid, self.lsb_uid) = struct.unpack("<QL", str_uid)
        # disable pagination to avoid gdb waiting for user input
        gdb.execute("set pagination off") 
        gdb.execute("target remote localhost:3333") 
        gdb.execute("monitor reset halt") 
        gdb.execute("monitor flash erase_address 0x08000000 0x00100000") 
        gdb.execute("monitor reset halt") 
        gdb.execute("file binaries/UNIT_TEST") 
        gdb.execute("load") 
        gdb.execute("monitor reset halt") 
        gdb.Breakpoint(self.BREAKPOINT_UID) 
        gdb.Breakpoint(self.BREAKPOINT_BLE) 
        gdb.Breakpoint(self.BREAKPOINT_END) 
        gdb.execute("monitor arm semihosting enable") 
        gdb.execute("continue") 
     
    def process_breakpoint(self, event): 
        if not isinstance(event, gdb.BreakpointEvent): 
            return False 
 
        if event.breakpoint.location == self.BREAKPOINT_UID: 
            gdb.execute("set variable UID[0] = %s" % hex(self.msb_uid)) 
            gdb.execute("set variable UID[1] = %s" % hex(self.lsb_uid)) 
            gdb.execute("continue") 
            return True # do we need to return here ? 
        elif event.breakpoint.location == self.BREAKPOINT_BLE: 
            mp = MicrochipProgrammer("/dev/ttyUSB0") 
            mp.connect() 
            mp.erase_flash(0, 0) 
 
            files = [ "bm71_data/BLEDK3_v111_c2826.H00", 
                    "bm71_data/BLEDK3_v111_c2826.H01", 
                    "bm71_data/BLEDK3_v111_c2826.H02", 
                    "bm71_data/BLEDK3_v111_c2826.H03", 
                  ] 
                   
            mp.flash_device(files) 
            mp.disconnect()

            time.sleep(0.5)

            mp.connect() 
            mp.erase_flash(0x34000, 0x2000) 
            mp.configure_device("bm71_data/bm71_config_file.txt") 
            mp.disconnect()
 
            gdb.execute("continue")
            return True # return needed ? 
        elif event.breakpoint .location == self.BREAKPOINT_END: 
            gdb.execute("monitor flash erase_address 0x08000000 0x00100000") 
            gdb.execute("monitor reset halt")
            gdb.execute("clear")
            gdb.execute("file {0}".format(self.program))
            gdb.execute("load")
            gdb.execute("monitor reset") 
            gdb.execute("disconnect")
            gdb.execute("quit")
            sys.exit(0)
            return False # return needed ? 
        else: 
            raise AssertionError('Unexpected event.')
            
if __name__ == "__main__":   
    builder = SerenityBuilder()
    builder.start_production(uid)
