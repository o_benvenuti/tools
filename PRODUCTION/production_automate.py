# imports
import Queue
import subprocess 
import Tkinter

# aliased imports
import ScrolledText as tkst 
import tkMessageBox as messagebox

# selected module imports
from threading import Thread

PRODUCTION_APP_VERSION = 0.1 
 
class OpenOCDServer(Thread): 
    """ That class start a threat which start a subprocess itself.
    The thread is usefull as it reads subprocess output and send it to
    the UI through a queue
    """ 
    def __init__(self, queue): 
        # Set up the background processing thread. 
        Thread.__init__(self) 
        self.queue = queue
        self.process = None

    def run(self): 
        """Thread main routine that run openocd instance and read log""" 
        self.process = subprocess.Popen([ "openocd", 
                    "-f", "/usr/local/share/openocd/scripts/interface/stlink-v2.cfg", 
                    "-f", "/usr/local/share/openocd/scripts/target/stm32l4x.cfg", 
                    "-s", "/usr/local/share/openocd/scripts/"], 
                    stdout=subprocess.PIPE) 
        # read log and send it to UI through provided queue 
        while self.process.poll() == None: 
            output = self.process.stdout.read(1) 
            if output != '': 
                self.queue.put(output)
    
    def release(self):
        try:
            self.process.terminate()
        except (OSError, AttributeError):
            print 'OpenOCD not running. Continue.'
 
 
class UserInterface(object): 
    """Top level class that manage UI and create all instances""" 
    def __init__(self, master): 
        self._uid = '' 
        self.master = master 
        self.builder = None
         
        # initialize queue and start openocd 
        self.queue = Queue.Queue() 
        self.openocd = OpenOCDServer(self.queue)
        self.openocd.start() 
        
        # create two main frames for log and control 
        self.log_frame = Tkinter.Frame(master, borderwidth=2, background='white') 
        self.log_frame.pack(side=Tkinter.RIGHT, fill='y', padx=5) 
        self.ctrl_frame = Tkinter.Frame(master, borderwidth=2, background='white') 
        self.ctrl_frame.pack(side=Tkinter.TOP, anchor=Tkinter.W, padx=5) 


        # create logo area 
        self.photo = Tkinter.PhotoImage(file="LOGO_SERENITY_CONCEPT.gif") 
        canvas = Tkinter.Canvas(self.ctrl_frame, width=200, height=140, background='white') 
        canvas.create_image(0, 0, anchor=Tkinter.NW, image=self.photo) 
        canvas.grid(row=0, sticky=Tkinter.W, pady=5)
        
        self.version_box = Tkinter.Label(self.ctrl_frame, borderwidth=1, relief="solid", 
            width=25,  anchor=Tkinter.W, text="Version: {0}".format(PRODUCTION_APP_VERSION)) 
        self.version_box.grid(row=1, sticky=Tkinter.W, pady=5, ipadx=2, ipady=2) 
         
        self.unique_box = Tkinter.Label(self.ctrl_frame, borderwidth=1, relief="solid", 
                                width=25, anchor=Tkinter.W, text="Batch n: ") 
        self.unique_box.grid(row=2, sticky=Tkinter.W, pady=5, ipadx=2, ipady=2) 
         
        self.status_box = Tkinter.Label(self.ctrl_frame, borderwidth=1, relief="solid", 
                                width=25, anchor=Tkinter.W,  text="Status: Waiting for UID.") 
        self.status_box.grid(row=3, sticky=Tkinter.W, pady=5, ipadx=2, ipady=2) 

        self.button_new = Tkinter.Button(self.ctrl_frame, text="NEW DEVICE", command=self.new_device)
        self.button_new.grid(row=4, sticky=Tkinter.W, pady=5, ipadx=2, ipady=2)
        
        # bind key input to local handler 
        self._key_bind = self.master.bind('<Key>', self.read_barcode) 
 
        # add a scrolled area for logs 
        self.log_text = tkst.ScrolledText(self.log_frame, width=75) 
        self.log_text.grid(sticky=Tkinter.N, pady=50) 

        # attach handler to kill subprocesses on exit
        master.protocol("WM_DELETE_WINDOW", self.on_closing)
 
        # call log function which will call itself after a delay 
        self.TimerInterval = 500 
        self.read_log()

    def new_device(self):
        """New device button handler. Kill previously started production
        if needed and reset labels and scrolled text area.
        """
        if self.builder != None and self.builder.poll() == None:
            if messagebox.askokcancel("Restart", "Already processing a device. Restart anyway ?"):
                self.builder.terminate()
                self.builder = None
            else:
                return
                
        self.unique_box['text'] = 'Batch n: '
        self.status_box['text'] = 'Status: Waiting for UID.'
        self.log_text.delete('1.0', Tkinter.END)  
        self._key_bind = self.master.bind('<Key>', self.read_barcode) 
        
    def read_log(self): 
        """ Read log queue from openocd instance and print it""" 
        while self.queue.qsize(): 
            try: 
                info = self.queue.get_nowait() 
                self.log_text.insert(Tkinter.END, info) 
                self.log_text.see("end") 
            except Queue.Empty:  # Shouldn't happen. 
                pass

        if self.builder != None:
            if self.builder.poll() != None:
                 self.status_box['text'] = 'Status: Done !'
                 self.builder = None
 
        # Now repeat call 
        self.master.after(self.TimerInterval, self.read_log)  
 
    def read_barcode(self, event): 
        if event.char in "0123456789C": 
            self._uid += event.char 
            self.unique_box['text'] += event.char 
            if len(self._uid) == 12: 
                self.master.unbind('<Key>', self._key_bind) 
                self.log_text.insert(Tkinter.INSERT, "Starting production for device: %s\n" % self._uid) 
                self.log_text.see("end") 
                self.status_box['text'] = 'Status: Flashing device'
                self.builder = subprocess.Popen([ "arm-none-eabi-gdb",
                                              "-ex", "py uid = '{0}'".format(self._uid),
                                              "-x", "serenity_builder.py" ])
                self._uid = ''
                                              
        else: 
            print 'invalid entry', repr(event.char), event.char 

    def on_closing(self):
        if messagebox.askokcancel("Quit", "Do you want to quit?"):
            try:
                self.builder.terminate()
            except (OSError, AttributeError):
                print 'Builder not running, exiting...'
            self.openocd.release()
            root.destroy()        
 
# execute only if run as a script
if __name__ == "__main__": 
    # create main window 
    root = Tkinter.Tk() 
    root.wm_title("SERENITY PRODUCTION") 
    root.wm_geometry('800x600+0+0')
    root.configure(background='white')
    
    UserInterface(root) 
 
    root.mainloop()
