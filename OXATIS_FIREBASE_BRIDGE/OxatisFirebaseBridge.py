#!/usr/bin/python3
import smtplib, ssl
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

import imaplib

import re
import base64
import email
import os
import logging
import datetime

import pyrebase

import requests
from requests import Session
from requests.exceptions import HTTPError


class _User:
  """ Get user information from _EmailOxatisInterface class and set 
      user information into _BackOfficeFirebaseInterface class
  """  
  
  def __init__(self):    
    """ """
    self.Model = ''
    self.optioncode = ''
    self.OptionBottomTimer = ''
    self.OptionDeco = ''
    self.OptionApnea = ''
    self.UID = ''
    self.Name = ''
    self.FirstName = ''
    self.Tel = ''
    self.Email = ''
    self.Password_App = ''
    
  def check_user_info(self):
    """ This function allows to verify """
    # Check Value of the option code
    if (self.OptionBottomTimer == 'Non') :
    
      if (self.OptionDeco == 'Non'):
      
        if (self.OptionApnea == 'Non'):
          self.optioncode = "54F0E977"
        else:
          self.optioncode = "0EA20B47"
          
      else:
            
        if (self.OptionApnea == 'Non'):
          self.optioncode = "B4EFEA08"
        else:
          self.optioncode = "4A7E9A10"
    
    else:
    
      if (self.OptionDeco == 'Non'):      
        if (self.OptionApnea == 'Non'):
          self.optioncode = "54F0E977"
        else:
          self.optioncode = "B156CCFC"
      else:
        if (self.OptionApnea == 'Non'):
          self.optioncode = "CAB495ED"
        else:
          self.optioncode = "DEABD08C"

    # Convert response 'Non' to 'No' and 'Oui' to 'Yes'
    if (self.OptionBottomTimer == 'Non'):
      self.OptionBottomTimer = 'No'  
    else:      
      self.OptionBottomTimer = 'Yes' 
      
    # Convert response 'Non' to 'No' and 'Oui' to 'Yes'      
    if (self.OptionDeco == 'Non'):
      self.OptionDeco = 'No'  
    else:      
      self.OptionDeco = 'Yes' 
      
    # Convert response 'Non' to 'No' and 'Oui' to 'Yes'      
    if (self.OptionApnea == 'Non'):
      self.OptionApnea = 'No'  
    else:      
      self.OptionApnea = 'Yes' 
    
    # Check the UID, if incorrect send an email and stop the process (ask another request)
    uid_error = 0
    if (len(self.UID) != 12):
      uid_error = 1
             
    # Check the Password, if incorrect send an email and stop the process (ask another request)
    password_error = 0
    if (len(self.Password_App) < 6):
      password_error = 1
 
    return uid_error, password_error
      
  def return_user_info(self):
    """ """
    return self.Model, self.optioncode, self.OptionBottomTimer, self.OptionDeco, self.OptionApnea, self.UID, self.Name, self.FirstName, self.Tel, self.Email, self.Password_App
    
  def extract_user_info_from_email(self, msg_content):
    """ """
    tmp = msg_content[0]
    data = tmp[1].split()
    fsm_state = 'idle'
    temp = ['','','']
    data_length = len(data)
    line = 0
    data_idx = 0
    InfoExtractDone = 0
    
    # Read each line
    while line < data_length:

      # Outputs management
      if (InfoExtractDone == 1):
        if (fsm_state == 'Modelecommande'):
          temp[data_idx] = data[line].decode("utf-8")
          data_idx = data_idx + 1
          if (data_idx == 2):
            self.Model = temp[0] + ' ' + temp[1]
            fsm_state = 'idle'
            temp = ['','','']
            
        elif (fsm_state == 'ModeProdondimetre'):
          temp[data_idx] = data[line].decode("utf-8")
          data_idx = data_idx + 1
          if (data_idx == 1):
            self.OptionBottomTimer = temp[0]
            fsm_state = 'idle'
            temp = ['','','']
            
        elif (fsm_state == 'Algorithmededecompression'):
          temp[data_idx] = data[line].decode("utf-8")
          data_idx = data_idx + 1
          if (data_idx == 1):
            self.OptionDeco = temp[0]
            fsm_state = 'idle'
            temp = ['','','']
            
        elif (fsm_state == 'Modeapnee'):
          temp[data_idx] = data[line].decode("utf-8")
          data_idx = data_idx + 1
          if (data_idx == 1):
            self.OptionApnea = temp[0]
            fsm_state = 'idle'
            temp = ['','','']
            
        elif (fsm_state == 'Numerodeserie'):
          temp[data_idx] = data[line].decode("utf-8")
          data_idx = data_idx + 1
          if (data_idx == 1):
            self.UID = temp[0]
            fsm_state = 'idle'
            temp = ['','','']
            
        elif (fsm_state == 'Nom'):
          temp[data_idx] = data[line].decode("utf-8")
          data_idx = data_idx + 1
          if (data_idx == 1):
            self.Name = temp[0]
            fsm_state = 'idle'
            temp = ['','','']
            
        elif (fsm_state == 'Prenom'):
          temp[data_idx] = data[line].decode("utf-8")
          data_idx = data_idx + 1
          if (data_idx == 1):
            self.FirstName = temp[0]
            fsm_state = 'idle'
            temp = ['','','']
            
        elif (fsm_state == 'Telephone'):
          temp[data_idx] = data[line].decode("utf-8")
          data_idx = data_idx + 1
          if (data_idx == 1):
            self.Tel = temp[0]
            fsm_state = 'idle'
            temp = ['','','']
            
        elif (fsm_state == 'Email'):
          temp[data_idx] = data[line].decode("utf-8")
          data_idx = data_idx + 1
          if (data_idx == 1):
            self.Email = temp[0]
            fsm_state = 'idle'
            temp = ['','','']
            
        elif (fsm_state == 'Choixdunmotdepassepourlapplic'):
          temp[data_idx] = data[line].decode("utf-8")
          data_idx = data_idx + 1
          if (data_idx == 1):
            self.Password_App = temp[0]
            fsm_state = 'idle'
            temp = ['','','']
        else:
          fsm_state = 'idle'
          temp = ['','','']
  
      # State transitions
      if (data[line].decode("utf-8") == '01-Modelecommande'):
        temp = ['','','']
        InfoExtractDone = InfoExtractDone + 1
        fsm_state = 'Modelecommande'
        data_idx = 0      
      elif (data[line].decode("utf-8") == '02-Option-ModeProdondimetre'):
        temp = ['','','']
        fsm_state = 'ModeProdondimetre'
        data_idx = 0      
      elif (data[line].decode("utf-8") == '03-Option-Algorithmededecompression'):
        temp = ['','','']
        fsm_state = 'Algorithmededecompression'      
        data_idx = 0
      elif (data[line].decode("utf-8") == '04-Option-Modeapnee'):
        temp = ['','','']
        fsm_state = 'Modeapnee'  
        data_idx = 0            
      elif (data[line].decode("utf-8") == '05-Numerodeserie'):
        temp = ['','','']
        fsm_state = 'Numerodeserie'
        data_idx = 0       
      elif (data[line].decode("utf-8") == '06-Nom'):
        temp = ['','','']
        fsm_state = 'Nom'
        data_idx = 0     
      elif (data[line].decode("utf-8") == '07-Prenom'):
        temp = ['','','']
        fsm_state = 'Prenom'
        data_idx = 0      
      elif (data[line].decode("utf-8") == '08-Telephone'):
        temp = ['','','']
        fsm_state = 'Telephone'
        data_idx = 0      
      elif (data[line].decode("utf-8") == '09-Email'):
        temp = ['','','']
        fsm_state = 'Email'
        data_idx = 0      
      elif (data[line].decode("utf-8") == '10-Choixdunmotdepassepourlapplic'):
        temp = ['','','']
        fsm_state = 'Choixdunmotdepassepourlapplic'
        data_idx = 0      
      else:
        fsm_state = fsm_state
         
      line = line + 1
        
    
    self.Model = self.Model.replace('=09','')
    self.OptionBottomTimer = self.OptionBottomTimer.replace('=09','')
    self.OptionDeco = self.OptionDeco.replace('=09','')
    self.OptionApnea = self.OptionApnea.replace('=09','')
    self.UID = self.UID.replace('=09','')
    self.Name = self.Name.replace('=09','') 
    self.FirstName = self.FirstName.replace('=09','') 
    self.Tel = self.Tel.replace('=09','')
    self.Email = self.Email.replace('=09','')
    self.Password_App = self.Password_App.replace('=09','')
        


class _EmailOxatisInterface:
  """ This class allows to:
    - be connected to 'ssl0.ovh.net' and support@serenityconcept.com 
      email address
    - search an 'Unseen' email from 'noreplay@oxatis.com' with
      subject 'Formulaire de garantie'
    - fetch the email found
  """
  
  def __init__(self):    
    """ """
    srv = 'ssl0.ovh.net'
    port_imap = 993
    port_smtp = 465
    self.user = 'support@serenityconcept.com'
    self.pwd = '0/serenity'
    # imap server
    self.mail = imaplib.IMAP4_SSL(srv, port_imap)
    # smtp server
    context = ssl.create_default_context()
    self.mail_smtp = smtplib.SMTP_SSL('ssl0.ovh.net','465', context=context)   
  
  def mailbox_imap_auth(self):
    """ """
    self.mail.login(self.user, self.pwd)
    return self.mail
    
  def mailbox_imap_logout(self):
    """ """
    self.mail.close()
    self.mail.logout()
  
  def mailbox_smtp_auth(self):
    """ """
    self.mail_smtp.login(self.user, self.pwd)
    return self.mail_smtp
    
  def mailbox_smtp_logout(self):
    """ """
    self.mail_smtp.quit()
  
  def send_UID_Error(self, user_address):
    """ """
    msg_content = '<p>Bonjour,</p>'
    msg_content = msg_content + '<p>Le numéro UID saisi lors de la création de votre compte SERENITY CONCEPT est incorrect.</p>'
    msg_content = msg_content + '<p>Pour le trouver il vous faut: sélectionner le menu paramètres (la roue crantée).</p>'
    msg_content = msg_content + '<p>Puis défiler vers la droite jusqu\'au dernier écran.</p>'
    msg_content = msg_content + '<p>Le code composé de 12 caractères se situera alors en bas de cette page appelée Réinitialisation d\'usine.</p>'
    msg_content = msg_content + '<p>Merci de bien vouloir enregistrer à nouveau votre instrument de plongée.</p>'
    msg_content = msg_content + '<p>L\'équipe SERENITY CONCEPT vous remercie pour la confiance que vous lui accordez et se tient à votre entière disposition.</p>'
    msg_content = msg_content + '<p>Service SAV</p>'
    msg_content = msg_content + '<p>sav@serenityconcept.com</p>\n'

    message = MIMEText(msg_content, 'html', 'ISO-8859-1')
    
    message['From'] = 'Serenity Concept <support@serenityconcept.com>'
    message['To'] = user_address
    message['Cc'] = 'Serenity Concept <support@serenityconcept.com>'
    message['Subject'] = 'Serenity Concept - UID incorrect'
    
    msg_full = message.as_string()
    
    self.mail_smtp.sendmail('support@serenityconcept.com', 
                  [user_address, 'support@serenityconcept.com'],
                  msg_full)
  
  def send_App_Password_Error(self, user_address): 
    """ """
    msg_content = '<p>Bonjour,</p>'
    msg_content = msg_content + '<p>Le mot de passe saisi lors de la création de votre compte SERENITY CONCEPT est incorrect; celui-ci doit être composé de 6 caractères minimum.</p>'
    msg_content = msg_content + '<p>Merci de bien vouloir enregistrer à nouveau votre instrument de plongée.</p>'
    msg_content = msg_content + '<p>L\'équipe SERENITY CONCEPT vous remercie pour la confiance que vous lui accordez et se tient à votre entière disposition.</p>'
    msg_content = msg_content + '<p>Service SAV</p>'
    msg_content = msg_content + '<p>sav@serenityconcept.com</p>\n'
    
    message = MIMEText(msg_content, 'html', 'ISO-8859-1')
    
    message['From'] = 'Serenity Concept <support@serenityconcept.com>'
    message['To'] = user_address
    message['Cc'] = 'Serenity Concept <support@serenityconcept.com>'
    message['Subject'] = 'Serenity Concept - Problème de mot de passe'
    
    msg_full = message.as_string()
    
    self.mail_smtp.sendmail('support@serenityconcept.com', 
                  [user_address, 'support@serenityconcept.com'],
                  msg_full) 
  
  def send_Email_Address_Already_Used(self, user_address): 
    """ """
    msg_content = '<p>Bonjour,</p>'
    msg_content = msg_content + '<p>L\'adresse mail saisie lors de la création de votre compte SERENITY CONCEPT existe déjà.</p>'
    msg_content = msg_content + '<p>Une adresse mail peut être associée qu\'à un seul instrument.</p>'
    msg_content = msg_content + '<p>Merci de bien vouloir vous enregistrer avec une nouvelle adresse email.</p>'
    msg_content = msg_content + '<p>Service SAV</p>'
    msg_content = msg_content + '<p>sav@serenityconcept.com</p>\n'
    message = MIMEText(msg_content, 'html', 'ISO-8859-1')
    
    message['From'] = 'Serenity Concept <support@serenityconcept.com>'
    message['To'] = user_address
    message['Cc'] = 'Serenity Concept <support@serenityconcept.com>'
    message['Subject'] = 'Serenity Concept - Adresse email déjà existante'
    
    msg_full = message.as_string()
    
    self.mail_smtp.sendmail('support@serenityconcept.com', 
                  [user_address, 'support@serenityconcept.com'],
                  msg_full)
  
  def send_App_Account_Valid(self, user_address, user_pwd): 
    """ """
    usrpwd = user_pwd
    msg_content = '<p>Bonjour,</p>'
    msg_content = msg_content + '<p>Votre compte Plan N Dive vient d\'être créé et activé avec succès.</p>'
    msg_content = msg_content + '<p>Votre identifiant: {email_address}</p>'.format(email_address=user_address)
    msg_content = msg_content + '<p>Votre mot de passe: {user_password}</p>'.format(user_password=usrpwd)
    msg_content = msg_content + '<p>L\'équipe SERENITY CONCEPT vous remercie pour la confiance que vous lui accordez et vous souhaite d\'agréables plongées.</p>'
    msg_content = msg_content + '<p>Service SAV</p>'
    msg_content = msg_content + '<p>sav@serenityconcept.com</p>\n'
    message = MIMEText(msg_content, 'html', 'ISO-8859-1')
    
    message['From'] = 'Serenity Concept <support@serenityconcept.com>'
    message['To'] = user_address
    message['Cc'] = 'c.assouline@serenityconcept.com'
    message['Subject'] = 'Serenity Concept - Compte Plan N Dive actif'
    
    msg_full = message.as_string()
    
    self.mail_smtp.sendmail('support@serenityconcept.com', 
                  [user_address, 'support@serenityconcept.com'],
                  msg_full)

  
  def send_to_Admin_Problem_With_Script(self): 
    """ """
    msg_content = '<p>Attention probleme inscription</p>\n'
    message = MIMEText(msg_content, 'html', 'ISO-8859-1')
    
    message['From'] = 'Serenity Concept <support@serenityconcept.com>'
    message['To'] = 'olbenvenuti@gmail.com'
    message['Cc'] = 'Serenity Concept <support@serenityconcept.com>'
    message['Subject'] = 'Serenity Concept - Probleme script Oxatis Firebase bridge'
    
    msg_full = message.as_string()
    
    self.mail_smtp.sendmail('support@serenityconcept.com', 
                  ['olbenvenuti@gmail.com', 'support@serenityconcept.com'],
                  msg_full)
  
  def check_if_new_request(self):
    """ """    
    self.mail.select('INBOX')
    result, [data] = self.mail.search(None, '(FROM "noreply@oxatis.com" SUBJECT "Formulaire de garantie" Unseen)')
    msg_num = data.split()
 
    if (msg_num == []):      
      msg_num = 0

    return msg_num

  def get_user_info(self, msg_num):
    """ """
    # Allows to fetch the email content
    result, data = self.mail.fetch(msg_num[0], '(RFC822)')
    return data
  
  def change_email_flag_to_seen(self, msg_num):
    """ """
    # Allows to change the email status from 'Unseen' to 'Seen'
    typ, data = self.mail.store(msg_num[0],'+FLAGS','\\Seen')
    
class _BackOfficeFirebaseInterface:
  """ This class allows to:
    - get user data (email, password, watch_uid and option code) 
      to create a new friebase account
    - create a new account into Authenticate partition
    - create the user account into the database
  """
  
  def __init__(self, model, optioncode, BottomTimer, Deco, Apnea, uid, name, firstname, phone, email, password, date):    
    """ """
    config = {
      "apiKey": "AIzaSyBNKvjxhx_ElNFXTG8CTSbJdBPOrcGMI9Y",
      "authDomain": "serenity-caeb1.firebaseapp.com",
      "databaseURL": "https://serenity-caeb1.firebaseio.com",
      "projectId": "serenity-caeb1",
      "storageBucket": "serenity-caeb1.appspot.com",
      "messagingSenderId": "394837651672"
      }
    
    self.name_user = name
    self.watch_model = model
    self.firstname_user = firstname
    self.bottomtimer = BottomTimer
    self.deco = Deco
    self.apnea = Apnea
    self.email_user = email
    self.phone_user = phone
    self.password_user = password
    self.datetoday = date
    self.true_uid = uid    
    self.watchuid_user = "".join("%02x" % ord(c) for c in uid)
    self.optioncode_user = optioncode
    
    firebase = pyrebase.initialize_app(config)
    self.auth = firebase.auth()
    self.db = firebase.database()
    
  def set_a_new_user_into_database(self):
    """ """
    try:
      user = self.auth.create_user_with_email_and_password(self.email_user, self.password_user)
      user_uid = user['localId']
      data = {
        self.watchuid_user : self.optioncode_user,
        "I0-UID" : self.true_uid,
        "I1-Model" : self.watch_model,
        "I2-option Bottom Timer" : self.bottomtimer,
        "I3-option Deco Algorithm" : self.deco,
        "I4-option Apnea" : self.apnea,      
        "I5-Name" : self.name_user,
        "I6-First Name" : self.firstname_user,
        "I7-email" : self.email_user,
        "I8-Password" : self.password_user,
        "I9-Account Creation Date" : self.datetoday
      }      

      self.db.child("optionCodes").child(user_uid).set(data)
      # User account is active
      return 1

    except HTTPError as e:
      # Problem HTTP
      return 0

if __name__ == "__main__":
 

  # Initialize mailbox interface
  MailboxOVHInterface = _EmailOxatisInterface()
  
  # Initialize User process
  UserProcess = _User()
  
  # Mailbox imap and smtp authentification
  mail_imap = MailboxOVHInterface.mailbox_imap_auth()
  mail_smtp = MailboxOVHInterface.mailbox_smtp_auth()
  
  
  # Check if a new request is available
  msg_num = MailboxOVHInterface.check_if_new_request()
  
  # if a new request is available
  if (msg_num == 0):
    # Mailbox imap and smtp deconnect
    MailboxOVHInterface.mailbox_imap_logout()    
    MailboxOVHInterface.mailbox_smtp_logout() 
    # End of script without error
    exit(0)
    
  # Change the email flag from 'Unseen' to 'Seen'
  MailboxOVHInterface.change_email_flag_to_seen(msg_num)
  
  # Fetch the email content
  email_content = MailboxOVHInterface.get_user_info(msg_num) 
  
  # add a user data extraction function
  UserProcess.extract_user_info_from_email(email_content)
  
  # Check UID, Password and convert options to an optioncode 
  UID_ERROR, PASSWORD_ERROR = UserProcess.check_user_info()
  
  # Extract user information
  Model, optionCodes, Option_BottomTimer, Option_Deco, Option_Apnea, UID, Name, FirstName, Phone, Email, Password_App = UserProcess.return_user_info()    
  
  # If UID is incorrect, send an email to the user and admin
  if (UID_ERROR == 1):
    MailboxOVHInterface.send_UID_Error(Email)
    MailboxOVHInterface.send_to_Admin_Problem_With_Script()
    
  # If password is incorrect, send an email to the user and admin   
  if (PASSWORD_ERROR == 1):
    MailboxOVHInterface.send_App_Password_Error(Email)
    MailboxOVHInterface.send_to_Admin_Problem_With_Script()
  
  # If UID or password error then it's the end of script 
  if ((UID_ERROR == 1) or (PASSWORD_ERROR == 1)): 
    # Mailbox imap and smtp deconnect
    MailboxOVHInterface.mailbox_imap_logout()    
    MailboxOVHInterface.mailbox_smtp_logout()
    exit(0)
  
  # Account date creation
  AccountDate = datetime.date.today()  
  
  # Diplay user informations
  #print(AccountDate.strftime('%d/%m/%Y') + '/' + Model + '/' + optionCodes + '/' + UID + '/' + Name + '/' + FirstName + '/' + Phone + '/' + Email + '/' + Password_App)
  
  # Initialize Firebase interface
  FirebaseInterface = _BackOfficeFirebaseInterface(Model, optionCodes, Option_BottomTimer, Option_Deco, Option_Apnea, UID, Name, FirstName, Phone, Email, Password_App, AccountDate.strftime('%d/%m/%Y'))  
  AccountStatus = FirebaseInterface.set_a_new_user_into_database()
  
  # Check the account status
  # If the account creation failed
  if (AccountStatus == 0):
    MailboxOVHInterface.send_Email_Address_Already_Used(Email)
    MailboxOVHInterface.send_to_Admin_Problem_With_Script()
  # Otherwise send an email to indicate to the user that account is active 
  else:
    MailboxOVHInterface.send_App_Account_Valid(Email, Password_App)
    
  # Mailbox imap and smtp deconnect
  MailboxOVHInterface.mailbox_imap_logout()    
  MailboxOVHInterface.mailbox_smtp_logout()  
 
  # End Script  
